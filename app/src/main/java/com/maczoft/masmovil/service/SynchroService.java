package com.maczoft.masmovil.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ParseException;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.maczoft.masmovil.R;
import com.maczoft.masmovil.domain.Agenda;
import com.maczoft.masmovil.domain.Bodega;
import com.maczoft.masmovil.domain.Categoria;
import com.maczoft.masmovil.domain.Cliente;
import com.maczoft.masmovil.domain.DetallePedido;
import com.maczoft.masmovil.domain.ItemOferta;
import com.maczoft.masmovil.domain.ListaPrecio;
import com.maczoft.masmovil.domain.Oferta;
import com.maczoft.masmovil.domain.Pedido;
import com.maczoft.masmovil.domain.PrecioProducto;
import com.maczoft.masmovil.domain.Producto;
import com.maczoft.masmovil.domain.TipoCliente;
import com.maczoft.masmovil.domain.Unidad;
import com.maczoft.masmovil.domain.Usuario;
import com.maczoft.masmovil.repository.sqlite.AgendaDao;
import com.maczoft.masmovil.repository.sqlite.BodegaDao;
import com.maczoft.masmovil.repository.sqlite.CategoriaDao;
import com.maczoft.masmovil.repository.sqlite.ClienteDao;
import com.maczoft.masmovil.repository.sqlite.DetallePedidoDao;
import com.maczoft.masmovil.repository.sqlite.ItemOfertaDao;
import com.maczoft.masmovil.repository.sqlite.ListaPrecioDao;
import com.maczoft.masmovil.repository.sqlite.OfertaDao;
import com.maczoft.masmovil.repository.sqlite.PedidoDao;
import com.maczoft.masmovil.repository.sqlite.PrecioProductoDao;
import com.maczoft.masmovil.repository.sqlite.ProductoDao;
import com.maczoft.masmovil.repository.sqlite.TipoClienteDao;
import com.maczoft.masmovil.repository.sqlite.UnidadDao;
import com.maczoft.masmovil.repository.sqlite.UsuarioDao;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marco Tulio on 10/02/2017.
 */

public class SynchroService {

    public static final String URL_USUARIOS = "getusuarios.php";
    public static final String URL_UNIDADES = "getunidades.php";
    public static final String URL_TIPO_CLIENTES = "gettipoclientes.php";
    public static final String URL_OFERTAS = "getofertas.php";
    public static final String URL_LISTAS = "getlistas.php";
    public static final String URL_DETALLE_OFERTAS = "getdetalleofertas.php";
    public static final String URL_CLIENTES = "getclientes.php";
    public static final String URL_BODEGAS = "getbodegas.php";
    public static final String URL_AGENDAS = "getagendas.php";
    public static final String URL_PRODUCTOS = "getproductos.php";
    public static final String URL_PRECIOS = "getprecios.php";
    public static final String URL_CATEGORIAS = "getcategorias.php";
    public static final String URL_SEND_VISITAS = "persistagendas.php";
    public static final String URL_SEND_PEDIDOS = "persistpedidos.php";

    private Context mContext;
    private String serverUrl;
    private Integer bodega;
    private Integer usuario;

    public SynchroService(Context context) {
        this.mContext = context;
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        serverUrl = sharedPref.getString("pref_sync_primary_url", mContext.getString(R.string.url_synchro));
        SharedPreferences pref = mContext.getSharedPreferences(context.getString(R.string.global_prefs), Context.MODE_PRIVATE);
        usuario = pref.getInt(mContext.getString(R.string.global_prefs_id_user), 1);
        bodega = pref.getInt(mContext.getString(R.string.global_prefs_id_bodega), 1);
    }

    @Nullable
    private String requestSyncData(String urlRequest) {
        HttpURLConnection urlConnection;
        try {
            String parameters = "bodega=" + bodega + "&usuario=" + usuario;
            URL url = new URL(serverUrl + urlRequest);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestMethod("POST");

            OutputStreamWriter request = new OutputStreamWriter(urlConnection.getOutputStream());
            request.write(parameters);
            request.flush();
            request.close();
            int statusCode = urlConnection.getResponseCode();

            // 200 represents HTTP OK
            if (statusCode == 200) {
                BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    response.append(line);
                }
                return response.toString();
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.d("SynchroService", e.getLocalizedMessage());
        }
        return null;
    }

    @Nullable
    private String sendSyncData(String urlRequest, String data) {
        HttpURLConnection urlConnection;
        try {
            String parameters = "bodega=" + bodega + "&usuario=" + usuario + "&data=" + data;
            URL url = new URL(serverUrl + urlRequest);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setInstanceFollowRedirects(false);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("charset", "utf-8");
            urlConnection.setUseCaches(false);

            OutputStreamWriter request = new OutputStreamWriter(urlConnection.getOutputStream());
            request.write(parameters);
            request.flush();
            request.close();
            int statusCode = urlConnection.getResponseCode();

            // 200 represents HTTP OK
            if (statusCode == 200) {
                BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    response.append(line);
                }
                return response.toString();
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.d("SynchroService", e.getLocalizedMessage());
        }
        return null;
    }

    public String syncUsuarios() throws Exception {
        String response = requestSyncData(URL_USUARIOS);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                UsuarioDao usuarioDao = new UsuarioDao(mContext);
                JSONArray usuarios = jsonObj.getJSONArray("data");
                ArrayList<Usuario> users = new ArrayList<Usuario>();
                for (int i = 0; i < usuarios.length(); i++) {
                    JSONObject object = usuarios.getJSONObject(i);
                    Usuario user = new Usuario();
                    int id = object.getInt("id");
                    String nombre = object.getString("nombre");
                    String contrasena = object.getString("contrasena");
                    int bodega = object.getInt("bodega");
                    int vendedor = object.getInt("vendedor");
                    BigDecimal comision = new BigDecimal(
                            object.getString("comision"));
                    user.setId(id);
                    user.setNombre(nombre);
                    user.setContrasena(contrasena);
                    Bodega b = new Bodega();
                    b.setId(bodega);
                    user.setBodega(b);
                    user.setVendedor(vendedor);
                    user.setComision(comision);
                    users.add(user);
                }
                usuarioDao.truncate();
                usuarioDao.insert(users);
                return "Carga de usuarios exitosa";
            } else {
                throw new Exception("Error de sincronización de usuarios");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncUnidades() throws Exception {
        String response = requestSyncData(URL_UNIDADES);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                UnidadDao unidadDao = new UnidadDao(mContext);
                JSONArray unidades = jsonObj.getJSONArray("data");
                ArrayList<Unidad> unis = new ArrayList<Unidad>();
                for (int i = 0; i < unidades.length(); i++) {
                    JSONObject object = unidades.getJSONObject(i);
                    Unidad unidad = new Unidad();
                    int id = object.getInt("idUnidades");
                    String nombre = object.getString("Nombre");
                    String abreviatura = object.getString("Abreviatura");
                    String ubase = object.getString("U_Base");
                    unidad.setId(id);
                    unidad.setNombre(nombre);
                    unidad.setAbreviatura(abreviatura);
                    unidad.setUbase(new BigDecimal(ubase));
                    unis.add(unidad);
                }
                unidadDao.truncate();
                unidadDao.insert(unis);
                return "Carga de Unidades exitosa";
            } else {
                throw new Exception("Error de sincronización de unidades");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncTipoClientes() throws Exception {
        String response = requestSyncData(URL_TIPO_CLIENTES);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                TipoClienteDao tipoClienteDao = new TipoClienteDao(mContext);
                JSONArray tipos = jsonObj.getJSONArray("data");
                ArrayList<TipoCliente> types = new ArrayList<TipoCliente>();
                for (int i = 0; i < tipos.length(); i++) {
                    JSONObject object = tipos.getJSONObject(i);
                    TipoCliente tipo = new TipoCliente();
                    int id = object.getInt("idtipo_cliente");
                    String nombre = object.getString("Nombre");
                    tipo.setId(id);
                    tipo.setNombre(nombre);
                    types.add(tipo);
                }
                tipoClienteDao.truncate();
                tipoClienteDao.insert(types);
                return "Carga de tipos de clientes exitosa";
            } else {
                throw new Exception(
                        "Error de sincronización de tipos de clientes");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncOfertas() throws Exception {
        String response = requestSyncData(URL_OFERTAS);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                OfertaDao ofertaDao = new OfertaDao(mContext);
                JSONArray oferts = jsonObj.getJSONArray("data");
                ArrayList<Oferta> ofertas = new ArrayList<Oferta>();
                for (int i = 0; i < oferts.length(); i++) {
                    JSONObject object = oferts.getJSONObject(i);
                    Oferta o = new Oferta();
                    int id = object.getInt("id");
                    String nombre = object.getString("Nombre");
                    String descripcion = object.getString("Descripcion");
                    BigDecimal precio = new BigDecimal(
                            object.getString("Precio"));
                    int bodega = object.getInt("Bodega");
                    o.setId(id);
                    o.setNombre(nombre);
                    o.setDescripcion(descripcion);
                    o.setPrecio(precio);
                    Bodega b = new Bodega();
                    b.setId(bodega);
                    o.setBodega(b);
                    ofertas.add(o);
                }
                ofertaDao.truncate();
                ofertaDao.insert(ofertas);
                return "Carga de ofertas exitosa";
            } else {
                throw new Exception("Error de sincronización de ofertas");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncListaPrecio() throws Exception {
        String response = requestSyncData(URL_LISTAS);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                ListaPrecioDao listaPrecioDao = new ListaPrecioDao(mContext);
                JSONArray lists = jsonObj.getJSONArray("data");
                ArrayList<ListaPrecio> lista = new ArrayList<ListaPrecio>();
                for (int i = 0; i < lists.length(); i++) {
                    JSONObject object = lists.getJSONObject(i);
                    ListaPrecio list = new ListaPrecio();
                    int id = object.getInt("id");
                    String nombre = object.getString("nombre");
                    list.setId(id);
                    list.setNombre(nombre);
                    lista.add(list);
                }
                listaPrecioDao.truncate();
                listaPrecioDao.insert(lista);
                return "Carga de listas de precio exitosa";
            } else {
                throw new Exception(
                        "Error de sincronización de listas de precio");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncItemOfeta() throws Exception {
        String response = requestSyncData(URL_DETALLE_OFERTAS);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                ItemOfertaDao itemOfertaDao = new ItemOfertaDao(mContext);
                JSONArray oferts = jsonObj.getJSONArray("data");
                ArrayList<ItemOferta> ofertas = new ArrayList<ItemOferta>();
                for (int i = 0; i < oferts.length(); i++) {
                    JSONObject object = oferts.getJSONObject(i);
                    ItemOferta o = new ItemOferta();
                    int id = object.getInt("id");
                    int oferta = object.getInt("oferta");
                    int producto = object.getInt("producto");
                    BigDecimal precio = new BigDecimal(
                            object.getString("precio"));
                    BigDecimal cantidad = new BigDecimal(
                            object.getString("cantidad"));
                    int unidad = object.getInt("Unidad");
                    Oferta ofert = new Oferta();
                    ofert.setId(oferta);
                    Producto prod = new Producto();
                    prod.setId(producto);
                    Unidad uni = new Unidad();
                    uni.setId(unidad);
                    o.setId(id);
                    o.setOferta(ofert);
                    o.setProducto(prod);
                    o.setPrecio(precio);
                    o.setCantidad(cantidad);
                    o.setUnidad(uni);
                    ofertas.add(o);
                }
                itemOfertaDao.truncate();
                itemOfertaDao.insert(ofertas);
                return "Carga de item ofertas exitosa";
            } else {
                throw new Exception("Error de sincronización de item ofertas");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncClientes() throws Exception {
        String response = requestSyncData(URL_CLIENTES);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                ClienteDao clienteDao = new ClienteDao(mContext);
                JSONArray clients = jsonObj.getJSONArray("data");
                clienteDao.truncate();
                for (int i = 0; i < clients.length(); i++) {
                    JSONObject object = clients.getJSONObject(i);
                    Cliente c = new Cliente();
                    int id = object.getInt("id");
                    String nombre = object.getString("nombre");
                    String codigo = object.getString("codigo");
                    String nit = object.getString("nit");
                    String direccion = object.getString("direccion");
                    int activo = object.getInt("activo");
                    Integer listaprecio = object.getInt("listaprecio");
                    Integer tipocliente = object.getInt("tipocliente");
                    c.setId(id);
                    c.setNombre(nombre);
                    c.setNit(nit);
                    c.setCodigo(codigo);
                    c.setDireccion(direccion);
                    c.setActivo(activo == 1);
                    ListaPrecio l = new ListaPrecio();
                    l.setId(listaprecio);
                    TipoCliente t = new TipoCliente();
                    t.setId(tipocliente);
                    c.setListaPrecio(l);
                    c.setTipoCliente(t);
                    clienteDao.insert(c);
                }
                return "Carga de clientes exitosa";
            } else {
                throw new Exception("Error de sincronización de clientes");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncBodegas() throws Exception {
        String response = requestSyncData(URL_BODEGAS);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                BodegaDao bodegaDao = new BodegaDao(mContext);
                JSONArray bods = jsonObj.getJSONArray("data");
                ArrayList<Bodega> bodegas = new ArrayList<Bodega>();
                for (int i = 0; i < bods.length(); i++) {
                    JSONObject object = bods.getJSONObject(i);
                    Bodega bodega = new Bodega();
                    int id = object.getInt("id");
                    String nombre = object.getString("nombre");
                    bodega.setId(id);
                    bodega.setNombre(nombre);
                    bodegas.add(bodega);
                }
                bodegaDao.truncate();
                bodegaDao.insert(bodegas);
                return "Carga de bodegas exitosa";
            } else {
                throw new Exception("Error de sincronización de bodegas");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncAgendas() throws Exception {
        String response = requestSyncData(URL_AGENDAS);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                AgendaDao agendaDao = new AgendaDao(mContext);
                JSONArray agends = jsonObj.getJSONArray("data");
                ArrayList<Agenda> agendas = new ArrayList<Agenda>();
                for (int i = 0; i < agends.length(); i++) {
                    JSONObject object = agends.getJSONObject(i);
                    Agenda a = new Agenda();
                    int id = object.getInt("id");
                    int cliente = object.getInt("cliente");
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date fecha = null;
                    try {
                        java.util.Date date = format.parse(object
                                .getString("fecha"));
                        fecha = new Date(date.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String motivo = object.getString("motivoanula");
                    int usuario = object.getInt("vendedor");
                    Cliente cli = new Cliente();
                    cli.setId(cliente);
                    Usuario user = new Usuario();
                    user.setId(usuario);
                    a.setId(id);
                    a.setCliente(cli);
                    a.setFecha(new Date(fecha.getTime()));
                    a.setVisitado(false);
                    a.setUsuario(user);
                    a.setMotivoanula(motivo);
                    agendas.add(a);
                }
                agendaDao.truncate();
                agendaDao.insert(agendas);
                return "Carga de ofertas exitosa";
            } else {
                throw new Exception("Error de sincronización de ofertas");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncProductos() throws Exception {
        String response = requestSyncData(URL_PRODUCTOS);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                ProductoDao productoDao = new ProductoDao(mContext);
                JSONArray products = jsonObj.getJSONArray("data");
                productoDao.truncate();
                for (int i = 0; i < products.length(); i++) {
                    JSONObject object = products.getJSONObject(i);
                    Producto p = new Producto();
                    int id = object.getInt("id");
                    String nombre = object.getString("nombre");
                    String marca = object.getString("marca");
                    String codigo = object.getString("codigo");
                    BigDecimal existencia = new BigDecimal(
                            object.getString("existencia"));
                    BigDecimal existenciamin = BigDecimal.ZERO;
                    if (object.getString("existencia_minima") != null) {
                        if (!object.getString("existencia_minima").equals("null")) {
                            existenciamin = new BigDecimal(object.getString("existencia_minima"));
                        }
                    }
                    p.setId(id);
                    p.setNombre(nombre);
                    p.setMarca(marca);
                    p.setCodigo(codigo);
                    p.setExistencia(existencia);
                    p.setExistenciaMinima(existenciamin);
                    productoDao.insert(p);
                }
                return "Carga de productos exitosa";
            } else {
                throw new Exception("Error de sincronización de productos");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncPrecioProductos() throws Exception {
        String response = requestSyncData(URL_PRECIOS);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                PrecioProductoDao precioProductoDao = new PrecioProductoDao(mContext);
                JSONArray products = jsonObj.getJSONArray("data");
                precioProductoDao.truncate();
                for (int i = 0; i < products.length(); i++) {
                    JSONObject object = products.getJSONObject(i);
                    PrecioProducto p = new PrecioProducto();
                    int producto = object.getInt("Producto");
                    int unidad = object.getInt("Unidad");
                    int lista = object.getInt("Listas_Precio");
                    BigDecimal precio = new BigDecimal(
                            object.getString("Precio"));
                    Producto prod = new Producto();
                    prod.setId(producto);
                    Unidad uni = new Unidad();
                    uni.setId(unidad);
                    ListaPrecio lis = new ListaPrecio();
                    lis.setId(lista);
                    p.setPrecio(precio);
                    p.setProducto(prod);
                    p.setListaPrecio(lis);
                    p.setUnidad(uni);
                    precioProductoDao.insert(p);
                }
                return "Carga de precios exitosa";
            } else {
                throw new Exception("Error de sincronización de precios");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String syncCategoriaProductos() throws Exception {
        String response = requestSyncData(URL_CATEGORIAS);
        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                CategoriaDao categoriaDao = new CategoriaDao(mContext);
                JSONArray cats = jsonObj.getJSONArray("data");
                categoriaDao.truncate();
                for (int i = 0; i < cats.length(); i++) {
                    JSONObject object = cats.getJSONObject(i);
                    Categoria c = new Categoria();
                    int id = object.getInt("id");
                    String nombre = object.getString("nombre");
                    c.setId(id);
                    c.setNombre(nombre);
                    categoriaDao.insert(c);
                }
                cats = jsonObj.getJSONArray("data2");
                categoriaDao.executUpdate("DELETE FROM producto_categoria");
                for (int i = 0; i < cats.length(); i++) {
                    JSONObject object = cats.getJSONObject(i);
                    int cate = object.getInt("categoria");
                    int prod = object.getInt("producto");
                    categoriaDao.executUpdate("INSERT INTO producto_categoria "
                            + "(producto, categoria) VALUES (" + prod + ","
                            + cate + ")");
                }
                return "Carga de categorías exitosa";
            } else {
                throw new Exception("Error de sincronización de categorías");
            }
        } else {
            throw new Exception("Error de conexión");
        }
    }

    public String sendVisitas() throws Exception {
        AgendaDao agendaDao = new AgendaDao(mContext);
        List<Agenda> agendas = agendaDao.getVisitados(usuario);

        JSONObject json = new JSONObject();
        int i = 0;
        for (Agenda a : agendas) {
            JSONObject jsonagenda = new JSONObject();
            if (a.getPedido() == null) {
                jsonagenda.put("motivo_anula", a.getMotivoanula());
                jsonagenda.put("idvisitas", a.getId());
                jsonagenda.put("hora_inicio", a.getHora().toString());
                jsonagenda.put("hora_fin", a.getHora().toString());
                jsonagenda.put("estado", "CANC");
                json.put("agenda" + i, jsonagenda);
                i++;
            }
        }

        String response = sendSyncData(URL_SEND_VISITAS, json.toString());

        if (response != null) {
            JSONObject jsonObj = new JSONObject(response);
            int success = jsonObj.getInt("success");
            if (success == 1) {
                for (Agenda a : agendas) {
                    if (a.getPedido() == null) {
                        agendaDao.delete(a);
                    }
                }
            }
        }
        return response;
    }

    public String sendPedidos() throws Exception {
        PedidoDao pedidoDao = new PedidoDao(mContext);
        List<Pedido> pedidos = pedidoDao.getPorEnviar(usuario);
        DetallePedidoDao detallePedidoDao = new DetallePedidoDao(mContext);
        AgendaDao agendaDao = new AgendaDao(mContext);

        JSONObject json = new JSONObject();

        int i = 0;
        for (Pedido p : pedidos) {
            JSONObject jsonpedido = new JSONObject();
            List<DetallePedido> detalles = detallePedidoDao.getByPedido(p);
            jsonpedido.put("fecha",
                    new java.sql.Date(p.getFecha().getTime()).toString());
            jsonpedido.put("cliente", p.getCliente().getId());
            jsonpedido.put("vendedor", p.getUsuario().getId());
            jsonpedido.put("total_lineas", detalles.size());
            jsonpedido.put("hora_inicio", new java.sql.Timestamp(p
                    .getHoraInicio().getTime()).toString());
            jsonpedido.put("subtotal", p.getTotal());
            jsonpedido.put("descuento", BigDecimal.ZERO);
            jsonpedido.put("total", p.getTotal());
            jsonpedido.put("observaciones", p.getObservaciones() == null ? " " : p.getObservaciones());
            jsonpedido.put("bodega", 1);
            jsonpedido.put("usuario", p.getUsuario().getId());
            JSONObject jsondetalles = new JSONObject();
            for (DetallePedido d : detalles) {
                JSONObject jsondetalle = new JSONObject();
                jsondetalle.put("producto", d.getProducto().getId());
                jsondetalle.put("cantidad", d.getCantidad());
                jsondetalle.put("precio", d.getPrecio());
                jsondetalle.put("unidad", d.getUnidad().getId());
                jsondetalle.put("descuento", BigDecimal.ZERO);
                jsondetalles.put("detalle" + d.getId(), jsondetalle);
            }
            jsonpedido.put("detalles", jsondetalles);
            Agenda agenda = agendaDao.getByPedido(p.getId());
            if (agenda != null) {
                JSONObject jsonagenda = new JSONObject();
                jsonagenda.put("horainicio", new java.sql.Timestamp(p
                        .getHoraInicio().getTime()).toString());
                jsonagenda.put("horafin", new java.sql.Timestamp(p
                        .getHoraFinal().getTime()).toString());
                jsonagenda.put("estado", "FINAL");
                jsonagenda.put("visita", agenda.getId());
                jsonpedido.put("agenda", jsonagenda);
            }

            json.put("pedido" + i, jsonpedido);
            i++;
        }

        String response = sendSyncData(URL_SEND_PEDIDOS, json.toString());

        if (response != null) {
            if (!response.contains("ERROR")) {
                for (Pedido p : pedidos) {
                    p.setEnviado(true);
                    pedidoDao.update(p);
                    Agenda agenda = agendaDao.getByPedido(p.getId());
                    if (agenda != null)
                        agendaDao.delete(agenda);
                }
            } else {
                Log.e("Synchro", response);
                throw new Exception("Error persistiendo pedidos");
            }
        }
        return response;
    }

}
