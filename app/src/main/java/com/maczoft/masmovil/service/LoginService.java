package com.maczoft.masmovil.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.maczoft.masmovil.R;
import com.maczoft.masmovil.domain.Usuario;
import com.maczoft.masmovil.repository.sqlite.UsuarioDao;
import com.maczoft.masmovil.util.MD5;

public class LoginService {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private UsuarioDao usuarioDao;
    private Context context;

    public LoginService(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(context.getString(R.string.global_prefs), Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public Usuario login(String usuario, String contrasena) {
        usuarioDao = new UsuarioDao(context);
        String md5Pass = MD5.getMD5(contrasena);
        Usuario loguser = null;
        try {
            loguser = usuarioDao.getByUsuarioContrasena(usuario, md5Pass);
        } catch (Exception e) {

        }
        if (loguser != null) {
            if (!isLogged()) {
                editor.putBoolean(context.getString(R.string.global_prefs_login), true);
                editor.putString(context.getString(R.string.global_prefs_user), loguser.getNombre());
                editor.putInt(context.getString(R.string.global_prefs_id_user), loguser.getId());
                editor.putInt(context.getString(R.string.global_prefs_id_bodega), loguser.getBodega().getId());
                editor.commit();
            }
        } else {
            logout();
        }
        return loguser;
    }

    public void logout() {
        editor.putBoolean(context.getString(R.string.global_prefs_login), false);
        editor.remove(context.getString(R.string.global_prefs_user));
        editor.commit();
    }

    public boolean isLogged() {
        return pref.getBoolean(context.getString(R.string.global_prefs_login), false);
    }

    public boolean isAutologin() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean("pref_autologin", true);
    }
}
