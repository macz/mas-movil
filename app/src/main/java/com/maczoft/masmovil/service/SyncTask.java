package com.maczoft.masmovil.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.maczoft.masmovil.R;
import com.maczoft.masmovil.SyncActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Marco Linares on 14/02/2017.
 */

//TODO Refactor a string todos los mensajes de texto en esta clase
public class SyncTask extends AsyncTask<Void, String, List<String>> {

    private Context mContext;
    private SynchroService service;
    private SharedPreferences.Editor editor;
    private TextView statusMessage;

    public SyncTask(Context context, TextView statusMessage) {
        this.mContext = context;
        this.statusMessage = statusMessage;
        service = new SynchroService(mContext);
        SharedPreferences prefs =
                mContext.getSharedPreferences(mContext.getString(R.string.global_prefs), Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    @Override
    protected List<String> doInBackground(Void... params) {

        List<String> mensajes = new ArrayList<String>();

        try {
            publishProgress("1  % Conectando con el servidor");
            publishProgress("10 % Enviando datos de Pedidos");
            mensajes.add(service.sendPedidos());
            publishProgress("19 % Sincronizando usuarios");
            mensajes.add(service.syncUsuarios());
            publishProgress("23 % Sincronizando unidades");
            mensajes.add(service.syncUnidades());
            publishProgress("30 % Sincronizando ofertas");
            mensajes.add(service.syncOfertas());
            publishProgress("35 % Sincronizando listas de precio");
            mensajes.add(service.syncListaPrecio());
            publishProgress("39 % Sincronizando detalle de ofertas");
            mensajes.add(service.syncItemOfeta());
            publishProgress("42 % Sincronizando clientes");
            mensajes.add(service.syncClientes());
            publishProgress("49 % Sincronizando bodegas");
            mensajes.add(service.syncBodegas());
            publishProgress("55 % Sincronizando la agenda");
            mensajes.add(service.syncAgendas());
            publishProgress("63 % Sincronizando productos");
            mensajes.add(service.syncProductos());
            publishProgress("70 % Sincronizando precios");
            mensajes.add(service.syncPrecioProductos());
            publishProgress("74 % Sincronizando tipos de clientes");
            mensajes.add(service.syncTipoClientes());
            publishProgress("85 % Sincronizando categorias");
            mensajes.add(service.syncCategoriaProductos());
//				publishProgress("92% Sincronizando visitas");
//				pedidoPersistence.sendVisitas();
            publishProgress("99% Sincronizando parametros");
            Calendar c = Calendar.getInstance();
            editor.putString(mContext.getString(R.string.global_prefs_last_update), c.getTime().toString());
            editor.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            mensajes.add("Error de base de datos: " + e.getMessage());
            editor.putString(mContext.getString(R.string.global_prefs_last_update), "");
            editor.commit();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            mensajes.add("Error de proceso: " + e.getMessage());
            editor.putString(mContext.getString(R.string.global_prefs_last_update), "");
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            mensajes.add("Error de sincronizacion: " + e.getMessage());
            editor.putString(mContext.getString(R.string.global_prefs_last_update), "");
            editor.commit();
        }
        return mensajes;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        statusMessage.setText(values[0]);
    }

    @Override
    protected void onPostExecute(final List<String> success) {
        SyncActivity act = (SyncActivity) mContext;
        act.showProgress(false);
        Toast toast1;
        if (!success.get(success.size() - 1).contains("Error")) {
            Log.d("response", success.get(0));
            toast1 = Toast.makeText(mContext,
                    "Sincronizacion exitosa: " + success.get(0), Toast.LENGTH_LONG);
            toast1.show();
        } else {
            Log.d("error", success.get(0));
            toast1 = Toast.makeText(mContext,
                    "Error al ejecutar sincronizacion revise su conexion", Toast.LENGTH_LONG);
            toast1.show();
        }
    }

    @Override
    protected void onCancelled() {
        SyncActivity act = (SyncActivity) mContext;
        act.showProgress(false);
        Toast toast1 = Toast.makeText(mContext,
                "Proceso de sincronizacion cancelado", Toast.LENGTH_SHORT);
        toast1.show();
    }

}
