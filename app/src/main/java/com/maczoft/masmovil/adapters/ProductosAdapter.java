package com.maczoft.masmovil.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.maczoft.masmovil.R;
import com.maczoft.masmovil.domain.ListaPrecio;
import com.maczoft.masmovil.domain.PrecioProducto;
import com.maczoft.masmovil.domain.Producto;
import com.maczoft.masmovil.repository.sqlite.PrecioProductoDao;
import com.maczoft.masmovil.util.VibratorUtil;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class ProductosAdapter extends RecyclerView.Adapter<ProductosAdapter.CustomViewHolder> {

    List<Producto> datos;
    ListaPrecio lista;
    boolean seleccion;
    private Context mContext;

    public ProductosAdapter(Activity context, List<Producto> datos, ListaPrecio lista, boolean seleccion) {
        this.datos = datos;
        this.lista = lista;
        this.seleccion = seleccion;
        this.mContext = context;
    }

    @Override
    public ProductosAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_productos_row, null);
        ProductosAdapter.CustomViewHolder viewHolder = new ProductosAdapter.CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProductosAdapter.CustomViewHolder customViewHolder, int i) {
        final Producto item = datos.get(i);
        PrecioProductoDao dao = new PrecioProductoDao(mContext);
        final PrecioProducto precio = dao.getPrecio(lista, item);
        String unidad = "";
        Locale locale = Locale.getDefault();
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);

        if (precio != null) {
            customViewHolder.precioLabel.setText(precio.getListaPrecio().getNombre());
            customViewHolder.precio.setText(numberFormat.format(precio.getPrecio()));
        } else {
            customViewHolder.precioLabel.setText("");
            customViewHolder.precio.setText("");
        }
        customViewHolder.producto.setText(item.getNombre() + " " + item.getCodigo());
        customViewHolder.marca.setText(item.getMarca());
        customViewHolder.existencia.setText(mContext.getString(R.string.productos_existencia) + " " + item.getExistencia() + " " + unidad);
        Picasso.with(mContext).load(mContext.getString(R.string.url_products_thumbnail) + item.getId())
                .error(R.drawable.shop_placeholder)
                .placeholder(R.drawable.shop_placeholder)
                .into(customViewHolder.thumbnail);
        if (seleccion) {
            customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    VibratorUtil.vibrate(mContext, 50);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("PRODUCTO", item.getId());
                    if (precio != null) {
                        returnIntent.putExtra("PRECIO", precio.getPrecio().toString());
                        returnIntent.putExtra("UNIDAD", precio.getUnidad().getId());
                    }
                    Activity activity = (Activity) mContext;
                    activity.setResult(Activity.RESULT_OK, returnIntent);
                    activity.finish();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (null != datos ? datos.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView producto;
        protected TextView precio;
        protected TextView marca;
        protected TextView precioLabel;
        protected TextView existencia;
        protected ImageView thumbnail;

        public CustomViewHolder(View view) {
            super(view);
            this.producto = (TextView) view.findViewById(R.id.field_nombre_producto);
            this.precio = (TextView) view.findViewById(R.id.field_precio_producto);
            this.marca = (TextView) view.findViewById(R.id.field_marca);
            this.precioLabel = (TextView) view.findViewById(R.id.label_precio_producto);
            this.existencia = (TextView) view.findViewById(R.id.field_existencia);
            this.thumbnail = (ImageView) view.findViewById(R.id.thumbnail_productos);
        }
    }
}
