package com.maczoft.masmovil.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maczoft.masmovil.R;
import com.maczoft.masmovil.domain.Cliente;
import com.maczoft.masmovil.util.VibratorUtil;

import java.util.List;

public class ClientesAdapter extends RecyclerView.Adapter<ClientesAdapter.CustomViewHolder> {

    List<Cliente> datos;
    boolean seleccion;
    private Context mContext;

    public ClientesAdapter(Activity context, List<Cliente> datos, boolean seleccion) {
        this.datos = datos;
        this.seleccion = seleccion;
        this.mContext = context;
    }


    @Override
    public ClientesAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_clientes_row, null);
        ClientesAdapter.CustomViewHolder viewHolder = new ClientesAdapter.CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ClientesAdapter.CustomViewHolder customViewHolder, int i) {
        final Cliente item = datos.get(i);

        customViewHolder.nombre.setText(item.getNombre());
        customViewHolder.codigo.setText(" CÓDGIO: " + item.getCodigo() + "  NIT: " + item.getNit());
        customViewHolder.direccion.setText(item.getDireccion());

        if (seleccion) {
            customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    VibratorUtil.vibrate(mContext, 50);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("CLIENTE", item.getId());
                    Activity activity = (Activity) mContext;
                    activity.setResult(Activity.RESULT_OK, returnIntent);
                    activity.finish();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (null != datos ? datos.size() : 0);
    }


    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView nombre;
        protected TextView codigo;
        protected TextView direccion;

        public CustomViewHolder(View view) {
            super(view);
            this.nombre = (TextView) view.findViewById(R.id.field_nombre_cliente);
            this.codigo = (TextView) view.findViewById(R.id.field_codigo_cliente);
            this.direccion = (TextView) view.findViewById(R.id.field_direccion_cliente);
        }
    }
}
