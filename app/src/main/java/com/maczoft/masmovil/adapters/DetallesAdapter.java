package com.maczoft.masmovil.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.maczoft.masmovil.R;
import com.maczoft.masmovil.TomaPedidosActivity;
import com.maczoft.masmovil.domain.DetallePedido;
import com.maczoft.masmovil.repository.sqlite.DetallePedidoDao;
import com.maczoft.masmovil.repository.sqlite.ProductoDao;
import com.maczoft.masmovil.util.VibratorUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class DetallesAdapter extends RecyclerView.Adapter<DetallesAdapter.CustomViewHolder> {

    private static final int PENDING_REMOVAL_TIMEOUT = 1000; // 1sec

    boolean undoOn; // is undo on, you can turn it on from the toolbar menu
    HashMap<DetallePedido, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be
    List<DetallePedido> datos;
    List<DetallePedido> itemsPendingRemoval;
    boolean onBind;
    private Handler handler = new Handler(); // hanlder for running delayed runnables
    private Context mContext;

    public DetallesAdapter(Activity context, List<DetallePedido> datos) {
        this.datos = datos;
        itemsPendingRemoval = new ArrayList<>();
        this.mContext = context;
    }


    @Override
    public DetallesAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pedidos_row, null);
        DetallesAdapter.CustomViewHolder viewHolder = new DetallesAdapter.CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DetallesAdapter.CustomViewHolder customViewHolder, int i) {
        final DetallePedido item = datos.get(i);
        onBind = true;

        Locale locale = Locale.getDefault();
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);

        if (itemsPendingRemoval.contains(item)) {
            // we need to show the "undo" state of the row
            customViewHolder.itemView.setBackgroundColor(Color.RED);
            customViewHolder.producto.setVisibility(View.GONE);
            customViewHolder.cantidad.setVisibility(View.GONE);
            customViewHolder.precio.setVisibility(View.GONE);
            customViewHolder.cantidadPrecio.setVisibility(View.GONE);
            customViewHolder.unidad.setVisibility(View.GONE);
            customViewHolder.plus.setVisibility(View.GONE);
            customViewHolder.minus.setVisibility(View.GONE);
            customViewHolder.undoButton.setVisibility(View.VISIBLE);

            customViewHolder.undoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // user wants to undo the removal, let's cancel the pending task
                    Runnable pendingRemovalRunnable = pendingRunnables.get(item);
                    pendingRunnables.remove(item);
                    if (pendingRemovalRunnable != null)
                        handler.removeCallbacks(pendingRemovalRunnable);
                    itemsPendingRemoval.remove(item);
                    // this will rebind the row in "normal" state
                    notifyItemChanged(datos.indexOf(item));
                }
            });
        } else {
            // we need to show the "normal" state
            customViewHolder.itemView.setBackgroundColor(Color.WHITE);
            customViewHolder.producto.setVisibility(View.VISIBLE);
            customViewHolder.cantidad.setVisibility(View.VISIBLE);
            customViewHolder.precio.setVisibility(View.VISIBLE);
            customViewHolder.cantidadPrecio.setVisibility(View.VISIBLE);
            customViewHolder.unidad.setVisibility(View.VISIBLE);
            customViewHolder.plus.setVisibility(View.VISIBLE);
            customViewHolder.minus.setVisibility(View.VISIBLE);
            customViewHolder.producto.setText(item.getProducto().toString());
            customViewHolder.cantidad.setText(item.getCantidad().toString());
            customViewHolder.precio.setText(numberFormat.format(item.getPrecio().setScale(2, BigDecimal.ROUND_UP)));
            customViewHolder.cantidadPrecio.setText("x " + numberFormat.format(item.getPrecioUnitario().setScale(2, BigDecimal.ROUND_UP)));
            customViewHolder.unidad.setText(item.getUnidad().toString());
            customViewHolder.undoButton.setVisibility(View.GONE);
            customViewHolder.undoButton.setOnClickListener(null);
        }
        onBind = false;
    }

    @Override
    public int getItemCount() {
        return (null != datos ? datos.size() : 0);
    }

    public void addCantidad(View v, int position) {
        onBind = true;
        DetallePedido item = datos.get(position);
        if (item.getProducto().getExistencia().compareTo(item.getCantidad()) > 0) {
            item.setCantidad(item.getCantidad().add(BigDecimal.ONE));
            item.setPrecio(item.getPrecioUnitario().multiply(item.getCantidad()));
            notifyItemChanged(position);
            TomaPedidosActivity act = (TomaPedidosActivity) mContext;
            act.actualizarTotal();
            try {
                ProductoDao dao = new ProductoDao(mContext);
                dao.subtractExistencias(item.getProducto(), BigDecimal.ONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        onBind = false;
    }

    public void removeCantidad(View v, int position) {
        onBind = true;
        DetallePedido item = datos.get(position);
        if (item.getCantidad().compareTo(BigDecimal.ONE) > 0) {
            item.setCantidad(item.getCantidad().subtract(BigDecimal.ONE));
            item.setPrecio(item.getPrecioUnitario().multiply(item.getCantidad()));
            notifyItemChanged(position);
            TomaPedidosActivity act = (TomaPedidosActivity) mContext;
            act.actualizarTotal();
            try {
                ProductoDao dao = new ProductoDao(mContext);
                dao.addExistencias(item.getProducto(), BigDecimal.ONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        onBind = false;
    }

    public void setCantidad(Editable s, DetallesAdapter.CustomViewHolder v, int position) throws ParseException {
        if (!onBind) {
            String string = s.toString();
            if (!string.isEmpty()) {
                DetallePedido item = datos.get(position);
                BigDecimal anterior = item.getCantidad();
                DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                symbols.setGroupingSeparator(',');
                symbols.setDecimalSeparator('.');
                String pattern = "#,##0.0#";
                DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
                decimalFormat.setParseBigDecimal(true);
                BigDecimal cant = (BigDecimal) decimalFormat.parse(string);
                if ((cant.compareTo(BigDecimal.ZERO) > 0) && (cant.compareTo(item.getProducto().getExistencia()) <= 0)) {
                    item.setCantidad(cant);
                } else if (cant.compareTo(item.getProducto().getExistencia()) > 0) {
                    item.setCantidad(item.getProducto().getExistencia());
                } else {
                    item.setCantidad(BigDecimal.ONE);
                }
                try {
                    ProductoDao dao = new ProductoDao(mContext);
                    if (anterior.compareTo(item.getCantidad()) > 0) {
                        dao.addExistencias(item.getProducto(), anterior.subtract(item.getCantidad()));
                    } else if (anterior.compareTo(item.getCantidad()) < 0) {
                        dao.subtractExistencias(item.getProducto(), item.getCantidad().subtract(anterior));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                item.setPrecio(item.getPrecioUnitario().multiply(item.getCantidad()));
                Locale locale = Locale.getDefault();
                NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
                v.precio.setText(numberFormat.format(item.getPrecio().setScale(2, RoundingMode.UP)));
                TomaPedidosActivity act = (TomaPedidosActivity) mContext;
                act.actualizarTotal();
            }
        }
    }

    public boolean isUndoOn() {
        return undoOn;
    }

    public void setUndoOn(boolean undoOn) {
        this.undoOn = undoOn;
    }

    public void pendingRemoval(int position) {
        final DetallePedido item = datos.get(position);
        if (!itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.add(item);
            // this will redraw row in "undo" state
            notifyItemChanged(position);
            // let's create, store and post a runnable to remove the item
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(datos.indexOf(item));
                    TomaPedidosActivity act = (TomaPedidosActivity) mContext;
                    act.actualizarTotal();
                }
            };
            handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            pendingRunnables.put(item, pendingRemovalRunnable);
        }
    }

    public void remove(int position) {
        onBind = true;
        DetallePedido item = datos.get(position);
        if (itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.remove(item);
        }
        if (datos.contains(item)) {
            try {
                DetallePedidoDao dao = new DetallePedidoDao(mContext);
                dao.delete(datos.get(position));
                datos.remove(position);
                notifyItemRemoved(position);
                ProductoDao dao2 = new ProductoDao(mContext);
                dao2.addExistencias(item.getProducto(), item.getCantidad());
                TomaPedidosActivity act = (TomaPedidosActivity) mContext;
                act.actualizarTotal();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        onBind = false;
    }

    public boolean isPendingRemoval(int position) {
        DetallePedido item = datos.get(position);
        return itemsPendingRemoval.contains(item);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, TextWatcher {
        protected TextView producto;
        protected TextView precio;
        protected TextView cantidadPrecio;
        protected EditText cantidad;
        protected TextView unidad;
        protected ImageButton plus;
        protected ImageButton minus;
        protected Button undoButton;

        public CustomViewHolder(View view) {
            super(view);
            this.producto = (TextView) view.findViewById(R.id.producto_item);
            this.precio = (TextView) view.findViewById(R.id.precio_item);
            this.cantidadPrecio = (TextView) view.findViewById(R.id.cantidad_precio_item);
            this.cantidad = (EditText) view.findViewById(R.id.cantidadPicker);
            this.unidad = (TextView) view.findViewById(R.id.unidad_item);
            this.plus = (ImageButton) view.findViewById(R.id.add_cantidad);
            this.minus = (ImageButton) view.findViewById(R.id.remove_cantidad);
            this.undoButton = (Button) itemView.findViewById(R.id.undo_button);
            plus.setOnClickListener(this);
            minus.setOnClickListener(this);
            cantidad.addTextChangedListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == plus.getId()) {
                VibratorUtil.vibrate(mContext, 50);
                addCantidad(v, getLayoutPosition());
            } else if (v.getId() == minus.getId()) {
                VibratorUtil.vibrate(mContext, 50);
                removeCantidad(v, getLayoutPosition());
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            try {
                setCantidad(s, this, getLayoutPosition());
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("DetallesAdapter", e.getLocalizedMessage());
            }
        }
    }
}
