package com.maczoft.masmovil.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maczoft.masmovil.R;
import com.maczoft.masmovil.TomaPedidosActivity;
import com.maczoft.masmovil.domain.Pedido;
import com.maczoft.masmovil.util.VibratorUtil;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class PedidosAdapter extends RecyclerView.Adapter<PedidosAdapter.CustomViewHolder> {

    List<Pedido> datos;
    private Context mContext;

    public PedidosAdapter(Activity context, List<Pedido> datos) {
        this.datos = datos;
        this.mContext = context;
    }

    @Override
    public PedidosAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pedido_row, null);
        PedidosAdapter.CustomViewHolder viewHolder = new PedidosAdapter.CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PedidosAdapter.CustomViewHolder customViewHolder, int i) {
        final Pedido item = datos.get(i);

        Locale locale = Locale.getDefault();
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);

        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(mContext);
        String fecha = dateFormat.format(item.getFecha());

        if (item.getCliente() != null) {
            customViewHolder.cliente.setText("CLIENTE: " + item.getCliente().getNombre() + " - " + item.getCliente().getCodigo());
            customViewHolder.direccion.setText("DIRECCIÓN: " + item.getCliente().getDireccion());
        } else {
            customViewHolder.cliente.setText("CLIENTE: ");
            customViewHolder.direccion.setText("DIRECCIÓN: ");
        }
        customViewHolder.fecha.setText("FECHA: " + fecha);
        customViewHolder.total.setText(numberFormat.format(item.getTotal()));
        if (item.getTotal_lineas() > 1)
            customViewHolder.productos.setText(item.getTotal_lineas() + " PRODUCTOS");
        else
            customViewHolder.productos.setText(item.getTotal_lineas() + " PRODUCTO");

        if (item.getEnviado()) {
            customViewHolder.estado.setTextColor(ResourcesCompat.getColor(mContext.getResources(), R.color.colorAccent, null));
            customViewHolder.estado.setText("ESTADO: ENVIADO");
        } else {
            customViewHolder.estado.setTextColor(ResourcesCompat.getColor(mContext.getResources(), R.color.colorAlert, null));
            customViewHolder.estado.setText("ESTADO: POR ENVIAR");
        }

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VibratorUtil.vibrate(mContext, 50);
                Intent returnIntent = new Intent(mContext, TomaPedidosActivity.class);
                returnIntent.putExtra("PEDIDO", item.getId());
                returnIntent.putExtra("SELECTION_MODE", "EDIT");
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != datos ? datos.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView cliente;
        protected TextView direccion;
        protected TextView fecha;
        protected TextView total;
        protected TextView productos;
        protected TextView estado;

        public CustomViewHolder(View view) {
            super(view);
            this.cliente = (TextView) view.findViewById(R.id.field_cliente_pedido);
            this.direccion = (TextView) view.findViewById(R.id.field_direccion_pedido);
            this.fecha = (TextView) view.findViewById(R.id.field_fecha_pedido);
            this.total = (TextView) view.findViewById(R.id.field_total_pedido);
            this.productos = (TextView) view.findViewById(R.id.field_productos_pedido);
            this.estado = (TextView) view.findViewById(R.id.field_estado_pedido);
        }
    }
}
