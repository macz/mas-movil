package com.maczoft.masmovil.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.maczoft.masmovil.NewsActivity;
import com.maczoft.masmovil.R;
import com.maczoft.masmovil.util.HtmlCompact;
import com.maczoft.masmovil.util.VibratorUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Marco Linares on 4/02/2017.
 */

public class NewsViewAdapter extends RecyclerView.Adapter<NewsViewAdapter.CustomViewHolder> {
    private List<News> feedItemList;
    private Context mContext;

    public NewsViewAdapter(Context context, List<News> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.main_news_row, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        final News feedItem = feedItemList.get(i);

        //Render image using Picasso library
        if (!TextUtils.isEmpty(feedItem.getThumbnail())) {
            Picasso.with(mContext).load(feedItem.getThumbnail())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(customViewHolder.imageView);
        }

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VibratorUtil.vibrate(mContext, 50);
                Intent i = new Intent(mContext, NewsActivity.class);
                i.putExtra(String.valueOf(feedItem.getId()), false);
                mContext.startActivity(i);
            }
        });
        //Setting text view title
        customViewHolder.textView.setText(HtmlCompact.fromHtml(feedItem.getTitle()));
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView textView;

        public CustomViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.thumbnail_news);
            this.textView = (TextView) view.findViewById(R.id.title_news);
        }
    }
}
