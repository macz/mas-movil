package com.maczoft.masmovil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.maczoft.masmovil.service.SendTask;
import com.maczoft.masmovil.service.SyncTask;
import com.maczoft.masmovil.util.VibratorUtil;

public class SyncActivity extends AppCompatActivity {

    View syncForm;
    ProgressBar progressBar;
    LinearLayout status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_sync);
        setSupportActionBar(myToolbar);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        syncForm = findViewById(R.id.sync_form);
        progressBar = (ProgressBar) findViewById(R.id.sync_progress);
        status = (LinearLayout) findViewById(R.id.sync_status);
        initButtons();
    }

    private void initButtons() {
        Button syncButton = (Button) findViewById(R.id.sync_button_action);
        Button sendButton = (Button) findViewById(R.id.send_button_action);
        Button imageButton = (Button) findViewById(R.id.images_button_action);
        Button mapsButton = (Button) findViewById(R.id.maps_button_action);
        final TextView statusText = (TextView) findViewById(R.id.status_sync_progress);
        final Activity padre = this;

        syncButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VibratorUtil.vibrate(v.getContext(), 50);
                showProgress(true);
                SyncTask syncTask = new SyncTask(padre, statusText);
                syncTask.execute((Void) null);
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VibratorUtil.vibrate(v.getContext(), 50);
                showProgress(true);
                SendTask syncTask = new SendTask(padre, statusText);
                syncTask.execute((Void) null);
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VibratorUtil.vibrate(v.getContext(), 50);
                Snackbar.make(padre.findViewById(R.id.activity_sync), padre.getString(R.string.not_available), Snackbar.LENGTH_LONG).show();
                //TODO download images
            }
        });
        mapsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VibratorUtil.vibrate(v.getContext(), 50);
                Snackbar.make(padre.findViewById(R.id.activity_sync), padre.getString(R.string.not_available), Snackbar.LENGTH_LONG).show();
                //TODO Send maps
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        VibratorUtil.vibrate(this, 50);
        onBackPressed();
        return true;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            syncForm.setVisibility(show ? View.GONE : View.VISIBLE);
            syncForm.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    syncForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            status.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                    status.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            status.setVisibility(show ? View.VISIBLE : View.GONE);
            syncForm.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
