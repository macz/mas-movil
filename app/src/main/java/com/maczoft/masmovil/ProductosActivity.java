package com.maczoft.masmovil;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.maczoft.masmovil.adapters.ProductosAdapter;
import com.maczoft.masmovil.domain.ListaPrecio;
import com.maczoft.masmovil.domain.Producto;
import com.maczoft.masmovil.repository.sqlite.ListaPrecioDao;
import com.maczoft.masmovil.repository.sqlite.ProductoDao;
import com.maczoft.masmovil.util.VibratorUtil;

import java.util.List;

public class ProductosActivity extends AppCompatActivity {

    SearchView searchView;
    RecyclerView mRecyclerView;
    ProductosAdapter adapter;
    List<Producto> productos;
    ListaPrecio lista;
    ProductoDao dao;
    ListaPrecioDao daol;
    ProgressBar progressBar;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_productos);
        setSupportActionBar(myToolbar);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar_productos);

        Intent iin = getIntent();
        bundle = iin.getExtras();
        dao = new ProductoDao(this);
        daol = new ListaPrecioDao(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.item_list_productos);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        new ProductosActivity.LoadTask().execute();
    }

    @Override
    public boolean onSupportNavigateUp() {
        VibratorUtil.vibrate(this, 50);
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_search, menu);

        final MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        final Context mContext = this;

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    searchView.setIconified(true);
                    myActionMenuItem.collapseActionView();
                }
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.length() >= 3) {
                    try {
                        ProductoDao dao = new ProductoDao(mContext);
                        List<Producto> prods = dao.getProductosByTarget(s);
                        productos.clear();
                        productos.addAll(prods);
                        adapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }
        });
        return true;
    }

    public class LoadTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                if (bundle != null) {
                    Integer l;
                    l = (Integer) bundle.get(getString(R.string.default_list));
                    if (l != null) {
                        lista = daol.getById(l);
                    } else {
                        lista = daol.getDefault();
                    }
                }
                productos = dao.getAll();
                result = 1;
            } catch (Exception e) {
                Log.d("Productos", e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressBar.setVisibility(View.GONE);

            if (result == 1) {
                adapter = new ProductosAdapter(ProductosActivity.this, productos, lista, bundle.getBoolean(getString(R.string.selection_mode), false));
                mRecyclerView.setAdapter(adapter);
            } else {
                Toast.makeText(ProductosActivity.this, getString(R.string.error_cant_load_products), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
