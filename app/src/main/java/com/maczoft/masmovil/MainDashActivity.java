package com.maczoft.masmovil;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.maczoft.masmovil.adapters.News;
import com.maczoft.masmovil.adapters.NewsViewAdapter;
import com.maczoft.masmovil.service.LoginService;
import com.maczoft.masmovil.util.SideMenuNavigationListener;
import com.maczoft.masmovil.util.VibratorUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainDashActivity extends AppCompatActivity {

    private List<News> feedsList;
    private RecyclerView mRecyclerView;
    private NewsViewAdapter adapter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dash);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new SideMenuNavigationListener((DrawerLayout) findViewById(R.id.drawer_layout)));

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.global_prefs), MODE_PRIVATE);
        String user = sharedPref.getString(getString(R.string.global_prefs_user), "");
        String welcome = getString(R.string.side_welcome, user);
        View headerView = navigationView.getHeaderView(0);
        TextView navWelcome = (TextView) headerView.findViewById(R.id.welcomeSide);
        navWelcome.setText(welcome);

        mRecyclerView = (RecyclerView) findViewById(R.id.news_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        //String url = getString(R.string.url_feeds);
        Resources resources = getResources();
        String url = ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(R.drawable.splash) + '/' + resources.getResourceTypeName(R.drawable.splash) + '/' + resources.getResourceEntryName(R.drawable.splash);
        new DownloadTask().execute(url);
    }

    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("posts");
            feedsList = new ArrayList<>();

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                News item = new News();
                item.setTitle(post.optString("title"));
                item.setThumbnail(post.optString("thumbnail"));
                feedsList.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        VibratorUtil.vibrate(this, 50);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_dash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_salir) {
            LoginService s = new LoginService(this);
            s.logout();
            // After logout redirect user to Loing Activity
            Intent i = new Intent(this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            startActivity(i);
        } else if (id == R.id.action_pedidos) {
            Intent i = new Intent(this, TomaPedidosActivity.class);
            i.putExtra(getString(R.string.read_only), false);
            startActivity(i);
        } else if (id == R.id.action_productos) {
            Intent i = new Intent(this, ProductosActivity.class);
            i.putExtra(getString(R.string.selection_mode), false);
            startActivity(i);
        } else if (id == R.id.action_clientes) {
            Intent i = new Intent(this, ClientesActivity.class);
            i.putExtra(getString(R.string.selection_mode), false);
            startActivity(i);
        } else if (id == R.id.action_syncro) {
            Intent i = new Intent(this, SyncActivity.class);
            startActivity(i);
        }

        VibratorUtil.vibrate(this, 50);

        return super.onOptionsItemSelected(item);
    }

    public class DownloadTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            //TODO Implementar blog en el servidor para tomar sus datos en esta área
//            HttpURLConnection urlConnection;
//            try {
//                URL url = new URL(params[0]);
//                urlConnection = (HttpURLConnection) url.openConnection();
//                int statusCode = urlConnection.getResponseCode();
//
//                // 200 represents HTTP OK
//                if (statusCode == 200) {
//                    BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//                    StringBuilder response = new StringBuilder();
//                    String line;
//                    while ((line = r.readLine()) != null) {
//                        response.append(line);
//                    }
//                    parseResult(response.toString());
//                    result = 1; // Successful
//                } else {
//                    result = 0; //"Failed to fetch data!";
//                }
            feedsList = new ArrayList<>();
            News item = new News();
            item.setTitle("Bienvenido a MAS Movil - Licencia Autorizada a Casa Comercial de León");
            item.setThumbnail(params[0]);
            feedsList.add(item);
            result = 1;
//            } catch (Exception e) {
//                Log.d("MainDashNews", e.getLocalizedMessage());
//            }
            return result; //"Failed to fetch data!";
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressBar.setVisibility(View.GONE);

            if (result == 1) {
                adapter = new NewsViewAdapter(MainDashActivity.this, feedsList);
                mRecyclerView.setAdapter(adapter);
            } else {
                Toast.makeText(MainDashActivity.this, getString(R.string.error_cant_connect_feed), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
