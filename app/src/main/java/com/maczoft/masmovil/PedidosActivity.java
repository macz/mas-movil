package com.maczoft.masmovil;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.maczoft.masmovil.adapters.PedidosAdapter;
import com.maczoft.masmovil.domain.Pedido;
import com.maczoft.masmovil.repository.sqlite.PedidoDao;
import com.maczoft.masmovil.util.VibratorUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PedidosActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    RecyclerView mRecyclerView;
    ProgressBar progressBar;
    PedidosAdapter adapter;
    List<Pedido> pedidos;
    Date selectedDate;
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_pedido);
        setSupportActionBar(myToolbar);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar_pedidos);

        mRecyclerView = (RecyclerView) findViewById(R.id.item_list_pedido);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);

        new PedidosActivity.LoadTask().execute();
    }

    @Override
    public boolean onSupportNavigateUp() {
        VibratorUtil.vibrate(this, 50);
        onBackPressed();
        return true;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar myCalendar = Calendar.getInstance();
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        selectedDate = myCalendar.getTime();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter_enviados) {

        } else if (id == R.id.action_filter_fecha_del) {
            datePickerDialog.show();
            java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(this);
            String fecha = dateFormat.format(selectedDate);
            item.setTitle("DEL: " + fecha);
        } else if (id == R.id.action_filter_fecha_al) {
            datePickerDialog.show();
            java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(this);
            String fecha = dateFormat.format(selectedDate);
            item.setTitle("AL: " + fecha);
        }
        VibratorUtil.vibrate(this, 50);

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pedidos_filters, menu);
        Calendar myCalendar = Calendar.getInstance();
        selectedDate = myCalendar.getTime();
        datePickerDialog = new DatePickerDialog(this, PedidosActivity.this, myCalendar.YEAR, myCalendar.MONTH, myCalendar.DAY_OF_MONTH);
        MenuItem delItem = menu.findItem(R.id.action_filter_fecha_del);
        MenuItem alItem = menu.findItem(R.id.action_filter_fecha_al);
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(this);
        String fecha = dateFormat.format(myCalendar.getTime());
        delItem.setTitle("DEL: " + fecha);
        alItem.setTitle("AL: " + fecha);
        return true;
    }

    public class LoadTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                PedidoDao dao = new PedidoDao(PedidosActivity.this);
                //TODO agregar filtro de búsqueda por defecto
                pedidos = dao.getPedidosByFilters(null, null, null);
                result = 1;
            } catch (Exception e) {
                Log.d("Pedidos", e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressBar.setVisibility(View.GONE);

            if (result == 1) {
                adapter = new PedidosAdapter(PedidosActivity.this, pedidos);
                mRecyclerView.setAdapter(adapter);
            } else {
                Toast.makeText(PedidosActivity.this, getString(R.string.error_cant_load_pedidos), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
