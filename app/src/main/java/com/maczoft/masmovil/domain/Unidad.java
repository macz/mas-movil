/**
 *
 */
package com.maczoft.masmovil.domain;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author maczoe
 */
public class Unidad implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String nombre;
    private String abreviatura;
    private BigDecimal ubase;


    public Unidad() {

    }

    /**
     * @param id
     * @param nombre
     * @param abreviatura
     * @param ubase
     */
    public Unidad(Integer id, String nombre, String abreviatura,
                  BigDecimal ubase) {
        this.id = id;
        this.nombre = nombre;
        this.abreviatura = abreviatura;
        this.ubase = ubase;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the abreviatura
     */
    public String getAbreviatura() {
        return abreviatura;
    }

    /**
     * @param abreviatura the abreviatura to set
     */
    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    /**
     * @return the ubase
     */
    public BigDecimal getUbase() {
        return ubase;
    }

    /**
     * @param ubase the ubase to set
     */
    public void setUbase(BigDecimal ubase) {
        this.ubase = ubase;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Unidad)) {
            return false;
        }
        Unidad other = (Unidad) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }
}
