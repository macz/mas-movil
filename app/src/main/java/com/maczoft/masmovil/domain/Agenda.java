/**
 *
 */
package com.maczoft.masmovil.domain;

import java.io.Serializable;
import java.sql.Date;

/**
 * @author maczoe
 */
public class Agenda implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Integer id;
    private Cliente cliente;
    private Date fecha;
    private Boolean visitado;
    private String motivoanula;
    private Usuario usuario;
    private Integer pedido;
    private java.sql.Timestamp hora;


    public Agenda() {

    }

    /**
     * @param id
     * @param cliente
     * @param fecha
     */
    public Agenda(Integer id, Cliente cliente, Date fecha) {
        this.id = id;
        this.cliente = cliente;
        this.fecha = fecha;
        this.visitado = false;
    }

    /**
     * @return the hora
     */
    public java.sql.Timestamp getHora() {
        return hora;
    }

    /**
     * @param hora the hora to set
     */
    public void setHora(java.sql.Timestamp hora) {
        this.hora = hora;
    }

    /**
     * @return the pedido
     */
    public Integer getPedido() {
        return pedido;
    }

    /**
     * @param pedido the pedido to set
     */
    public void setPedido(Integer pedido) {
        this.pedido = pedido;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the motivoanula
     */
    public String getMotivoanula() {
        return motivoanula;
    }

    /**
     * @param motivoanula the motivoanula to set
     */
    public void setMotivoanula(String motivoanula) {
        this.motivoanula = motivoanula;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the visitado
     */
    public Boolean getVisitado() {
        return visitado;
    }

    /**
     * @param visitado the visitado to set
     */
    public void setVisitado(Boolean visitado) {
        this.visitado = visitado;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.fecha + " : " + this.cliente.toString();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Agenda)) {
            return false;
        }
        Agenda other = (Agenda) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }
}
