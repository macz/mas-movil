/**
 *
 */
package com.maczoft.masmovil.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author maczoe
 */
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String nombre;
    private String marca;
    private String codigo;
    private BigDecimal existencia;
    private BigDecimal existenciaMinima;
    private List<Categoria> categorias;

    public Producto() {

    }

    /**
     * @param id
     * @param nombre
     * @param marca
     * @param existencia
     */
    public Producto(Integer id, String nombre, String marca,
                    BigDecimal existencia) {
        this.id = id;
        this.nombre = nombre;
        this.marca = marca;
        this.existencia = existencia;
    }

    /**
     * @return the categorias
     */
    public List<Categoria> getCategorias() {
        return categorias;
    }

    /**
     * @param categorias the categorias to set
     */
    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    /**
     * @return the existenciaMinima
     */
    public BigDecimal getExistenciaMinima() {
        return existenciaMinima;
    }

    /**
     * @param existenciaMinima the existenciaMinima to set
     */
    public void setExistenciaMinima(BigDecimal existenciaMinima) {
        this.existenciaMinima = existenciaMinima;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the existencia
     */
    public BigDecimal getExistencia() {
        return existencia;
    }

    /**
     * @param existencia the existencia to set
     */
    public void setExistencia(BigDecimal existencia) {
        this.existencia = existencia;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }
}
