/**
 *
 */
package com.maczoft.masmovil.domain;

import java.io.Serializable;

/**
 * @author maczoe
 */
public class ReglaOferta implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String condicion;
    private String valor;
    private Producto producto;
    private Oferta oferta;

    public ReglaOferta() {
    }


    /**
     * @param id
     * @param condicion
     * @param valor
     * @param producto
     * @param oferta
     */
    public ReglaOferta(Integer id, String condicion, String valor,
                       Producto producto, Oferta oferta) {
        this.id = id;
        this.condicion = condicion;
        this.valor = valor;
        this.producto = producto;
        this.oferta = oferta;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the condicion
     */
    public String getCondicion() {
        return condicion;
    }

    /**
     * @param condicion the condicion to set
     */
    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    /**
     * @return the valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(String valor) {
        this.valor = valor;
    }

    /**
     * @return the producto
     */
    public Producto getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    /**
     * @return the oferta
     */
    public Oferta getOferta() {
        return oferta;
    }

    /**
     * @param oferta the oferta to set
     */
    public void setOferta(Oferta oferta) {
        this.oferta = oferta;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.condicion + " = " + this.valor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ReglaOferta)) {
            return false;
        }
        ReglaOferta other = (ReglaOferta) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }
}
