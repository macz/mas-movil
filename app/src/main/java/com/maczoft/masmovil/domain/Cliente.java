/**
 *
 */
package com.maczoft.masmovil.domain;

import java.io.Serializable;

/**
 * @author maczoe
 */
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    private String nombre;
    private String codigo;
    private String nit;
    private String direccion;
    private Boolean activo;
    private Integer id;
    private ListaPrecio listaPrecio;
    private TipoCliente tipoCliente;
    private String motivoInactivo;


    public Cliente() {

    }

    /**
     * @param nombre
     * @param nit
     * @param direccion
     * @param id
     */
    public Cliente(String nombre, String nit, String direccion, Integer id) {
        this.nombre = nombre;
        this.nit = nit;
        this.direccion = direccion;
        this.id = id;
    }

    /**
     * @return the motivoInactivo
     */
    public String getMotivoInactivo() {
        return motivoInactivo;
    }

    /**
     * @param motivoInactivo the motivoInactivo to set
     */
    public void setMotivoInactivo(String motivoInactivo) {
        this.motivoInactivo = motivoInactivo;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the activo
     */
    public Boolean getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    /**
     * @return the listaPrecio
     */
    public ListaPrecio getListaPrecio() {
        return listaPrecio;
    }

    /**
     * @param listaPrecio the listaPrecio to set
     */
    public void setListaPrecio(ListaPrecio listaPrecio) {
        this.listaPrecio = listaPrecio;
    }

    /**
     * @return the tipoCliente
     */
    public TipoCliente getTipoCliente() {
        return tipoCliente;
    }

    /**
     * @param tipoCliente the tipoCliente to set
     */
    public void setTipoCliente(TipoCliente tipoCliente) {
        this.tipoCliente = tipoCliente;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

}
