/**
 *
 */
package com.maczoft.masmovil.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;


/**
 * @author maczoe
 */
public class Pedido implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private Date fecha;
    private Cliente cliente;
    private Usuario usuario;
    private Integer total_lineas;
    private Timestamp horaInicio;
    private Timestamp horaFinal;
    private BigDecimal total;
    private String observaciones;
    private Boolean enviado;

    public Pedido() {

    }

    /**
     * @param id
     * @param fecha
     * @param horaInicio
     * @param horaFinal
     * @param total
     * @param observaciones
     * @param enviado
     */
    public Pedido(Integer id, Date fecha, Timestamp horaInicio,
                  Timestamp horaFinal, BigDecimal total, String observaciones,
                  Boolean enviado) {
        this.id = id;
        this.fecha = fecha;
        this.horaInicio = horaInicio;
        this.horaFinal = horaFinal;
        this.total = total;
        this.observaciones = observaciones;
        this.enviado = enviado;
    }

    /**
     * @return the total_lineas
     */
    public Integer getTotal_lineas() {
        return total_lineas;
    }

    /**
     * @param total_lineas the total_lineas to set
     */
    public void setTotal_lineas(Integer total_lineas) {
        this.total_lineas = total_lineas;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the horaInicio
     */
    public Timestamp getHoraInicio() {
        return horaInicio;
    }

    /**
     * @param horaInicio the horaInicio to set
     */
    public void setHoraInicio(Timestamp horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * @return the horaFinal
     */
    public Timestamp getHoraFinal() {
        return horaFinal;
    }

    /**
     * @param horaFinal the horaFinal to set
     */
    public void setHoraFinal(Timestamp horaFinal) {
        this.horaFinal = horaFinal;
    }

    /**
     * @return the total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * @return the observaciones
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * @param observaciones the observaciones to set
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    /**
     * @return the enviado
     */
    public Boolean getEnviado() {
        return enviado;
    }

    /**
     * @param enviado the enviado to set
     */
    public void setEnviado(Boolean enviado) {
        this.enviado = enviado;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "" + this.id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Pedido)) {
            return false;
        }
        Pedido other = (Pedido) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

}
