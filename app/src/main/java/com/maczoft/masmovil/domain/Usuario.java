/**
 *
 */
package com.maczoft.masmovil.domain;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author maczoe
 */
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String nombre;
    private String contrasena;
    private Bodega bodega;
    private BigDecimal comision;
    private Integer vendedor;

    public Usuario() {

    }

    /**
     * @param id
     * @param nombre
     * @param contrasena
     * @param comision
     */
    public Usuario(Integer id, String nombre, String contrasena, BigDecimal comision) {
        this.id = id;
        this.nombre = nombre;
        this.contrasena = contrasena;
        this.comision = comision;
    }

    /**
     * @return the vendedor
     */
    public Integer getVendedor() {
        return vendedor;
    }

    /**
     * @param vendedor the vendedor to set
     */
    public void setVendedor(Integer vendedor) {
        this.vendedor = vendedor;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the contrasena
     */
    public String getContrasena() {
        return contrasena;
    }

    /**
     * @param contrasena the contrasena to set
     */
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /**
     * @return the bodega
     */
    public Bodega getBodega() {
        return bodega;
    }

    /**
     * @param bodega the bodega to set
     */
    public void setBodega(Bodega bodega) {
        this.bodega = bodega;
    }

    /**
     * @return the comision
     */
    public BigDecimal getComision() {
        return comision;
    }

    /**
     * @param comision the comision to set
     */
    public void setComision(BigDecimal comision) {
        this.comision = comision;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }
}
