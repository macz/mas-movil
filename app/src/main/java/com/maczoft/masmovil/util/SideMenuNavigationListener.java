package com.maczoft.masmovil.util;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.widget.Toast;

import com.maczoft.masmovil.ClientesActivity;
import com.maczoft.masmovil.LoginActivity;
import com.maczoft.masmovil.ObjetivosActivity;
import com.maczoft.masmovil.PedidosActivity;
import com.maczoft.masmovil.ProductosActivity;
import com.maczoft.masmovil.R;
import com.maczoft.masmovil.SettingsActivity;
import com.maczoft.masmovil.SyncActivity;
import com.maczoft.masmovil.TomaPedidosActivity;
import com.maczoft.masmovil.VisitasActivity;
import com.maczoft.masmovil.service.LoginService;

/**
 * Created by Marco Linares on 3/02/2017.
 */

public class SideMenuNavigationListener implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;

    public SideMenuNavigationListener(DrawerLayout drawer) {
        this.drawer = drawer;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Toast toast = Toast.makeText(drawer.getContext(), drawer.getContext().getString(R.string.not_available), Toast.LENGTH_LONG);
        if (id == R.id.nav_agenda) {
            toast.show();
        } else if (id == R.id.nav_clientes) {
            Intent i = new Intent(drawer.getContext(), ClientesActivity.class);
            i.putExtra(drawer.getContext().getString(R.string.selection_mode), false);
            drawer.getContext().startActivity(i);
        } else if (id == R.id.nav_configurar) {
            Intent i = new Intent(drawer.getContext(), SettingsActivity.class);
            drawer.getContext().startActivity(i);
        } else if (id == R.id.nav_objetivos) {
            Intent i = new Intent(drawer.getContext(), ObjetivosActivity.class);
            drawer.getContext().startActivity(i);
        } else if (id == R.id.nav_pedidos) {
            Intent i = new Intent(drawer.getContext(), PedidosActivity.class);
            drawer.getContext().startActivity(i);
        } else if (id == R.id.nav_productos) {
            Intent i = new Intent(drawer.getContext(), ProductosActivity.class);
            i.putExtra(drawer.getContext().getString(R.string.selection_mode), false);
            drawer.getContext().startActivity(i);
        } else if (id == R.id.nav_resumen) {
            toast.show();
        } else if (id == R.id.nav_sincro) {
            Intent i = new Intent(drawer.getContext(), SyncActivity.class);
            drawer.getContext().startActivity(i);
        } else if (id == R.id.nav_toma_pedidos) {
            Intent i = new Intent(drawer.getContext(), TomaPedidosActivity.class);
            drawer.getContext().startActivity(i);
        } else if (id == R.id.nav_visitas) {
            Intent i = new Intent(drawer.getContext(), VisitasActivity.class);
            drawer.getContext().startActivity(i);
        } else if (id == R.id.nav_salir) {
            LoginService s = new LoginService(drawer.getContext());
            s.logout();
            // After logout redirect user to Loing Activity
            Intent i = new Intent(drawer.getContext(), LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            drawer.getContext().startActivity(i);
        }
        VibratorUtil.vibrate(drawer.getContext(), 50);

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
