package com.maczoft.masmovil.util;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Vibrator;
import android.preference.PreferenceManager;

/**
 * Created by Marco Linares on 6/02/2017.
 */

public class VibratorUtil {

    public static void vibrate(Context context, int milis) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        Boolean ok = sharedPref.getBoolean("pref_vibration", true);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            int permissionCheck = context.checkSelfPermission(Manifest.permission.VIBRATE);
            if (permissionCheck == PackageManager.PERMISSION_DENIED) {
                ok = false;
            }
        }
        if (ok) {
            Vibrator vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            vibe.vibrate(milis);
        }
    }
}
