package com.maczoft.masmovil;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.maczoft.masmovil.adapters.DetallesAdapter;
import com.maczoft.masmovil.domain.Cliente;
import com.maczoft.masmovil.domain.DetallePedido;
import com.maczoft.masmovil.domain.Pedido;
import com.maczoft.masmovil.domain.Producto;
import com.maczoft.masmovil.domain.Unidad;
import com.maczoft.masmovil.domain.Usuario;
import com.maczoft.masmovil.repository.sqlite.ClienteDao;
import com.maczoft.masmovil.repository.sqlite.DetallePedidoDao;
import com.maczoft.masmovil.repository.sqlite.PedidoDao;
import com.maczoft.masmovil.repository.sqlite.ProductoDao;
import com.maczoft.masmovil.repository.sqlite.UnidadDao;
import com.maczoft.masmovil.util.VibratorUtil;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class TomaPedidosActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PRODUCTO_SELECCION_CODE = 1;
    private static final int CLIENTE_SELECCION_CODE = 2;

    private RecyclerView detalleList;
    private DetallesAdapter adapter;
    private BigDecimal total;
    private Cliente cliente;
    private List<DetallePedido> detalles;
    private Pedido pedido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toma_pedidos);

        // Variables de inicialización del fórmulario
        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        boolean readOnly = false;
        if (b != null) {
            readOnly = (boolean) b.get(getString(R.string.read_only));
        }


        FloatingActionButton fabProductos = (FloatingActionButton) findViewById(R.id.floatingActionButtonProductos);
        fabProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibratorUtil.vibrate(view.getContext(), 50);
                buscarProducto();
            }
        });

        FloatingActionButton fabGuardar = (FloatingActionButton) findViewById(R.id.floatingActionButtonSave);
        fabGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibratorUtil.vibrate(view.getContext(), 50);
                guardarPedido();
            }
        });

        if (!readOnly) {
            Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_pedidos);
            setSupportActionBar(myToolbar);
        } else {
            fabGuardar.hide();
            fabProductos.hide();
        }

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        // Botones flotantes de acción guardar y buscar producto

        //Restaura pedido por defecto
        restoreDefaultPedido();
        restoreDetalles();

        // Lista de detalles
        detalleList = (RecyclerView) findViewById(R.id.item_list_pedidos);
        detalleList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new DetallesAdapter(this, detalles);
        detalleList.setAdapter(adapter);
        adapter.setUndoOn(false);
        detalleList.setHasFixedSize(true);
        setUpItemTouchHelper();
        setUpAnimationDecoratorHelper();

        TextView label = (TextView) this.findViewById(R.id.cliente_label);
        TextView nombre = (TextView) this.findViewById(R.id.cliente_field);
        TextView codigo = (TextView) this.findViewById(R.id.codigo_field);
        TextView label2 = (TextView) this.findViewById(R.id.codigo_label);
        label.setOnClickListener(this);
        nombre.setOnClickListener(this);
        codigo.setOnClickListener(this);
        label2.setOnClickListener(this);
    }

    /**
     * This is the standard support library way of implementing "swipe to delete" feature. You can do custom drawing in onChildDraw method
     * but whatever you draw will disappear once the swipe is over, and while the items are animating to their new position the recycler view
     * background will be visible. That is rarely an desired effect.
     */
    private void setUpItemTouchHelper() {

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            // we want to cache these and not allocate anything repeatedly in the onChildDraw method
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                xMark = ContextCompat.getDrawable(TomaPedidosActivity.this, R.drawable.ic_delete);
                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) TomaPedidosActivity.this.getResources().getDimension(R.dimen.ic_clear_margin);
                initiated = true;
            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                DetallesAdapter testAdapter = (DetallesAdapter) recyclerView.getAdapter();
                if (testAdapter.isUndoOn() && testAdapter.isPendingRemoval(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                DetallesAdapter adapter = (DetallesAdapter) detalleList.getAdapter();
                boolean undoOn = adapter.isUndoOn();
                if (undoOn) {
                    adapter.pendingRemoval(swipedPosition);
                } else {
                    adapter.remove(swipedPosition);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                // not sure why, but this method get's called for viewholder that are already swiped away
                if (viewHolder.getAdapterPosition() == -1) {
                    // not interested in those
                    return;
                }

                if (!initiated) {
                    init();
                }

                // draw red background
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                // draw x mark
                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth();
                int intrinsicHeight = xMark.getIntrinsicWidth();

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(detalleList);
    }

    /**
     * We're gonna setup another ItemDecorator that will draw the red background in the empty space while the items are animating to thier new positions
     * after an item is removed.
     */
    private void setUpAnimationDecoratorHelper() {
        detalleList.addItemDecoration(new RecyclerView.ItemDecoration() {

            // we want to cache this and not allocate anything repeatedly in the onDraw method
            Drawable background;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                initiated = true;
            }

            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {

                if (!initiated) {
                    init();
                }

                // only if animation is in progress
                if (parent.getItemAnimator().isRunning()) {

                    // some items might be animating down and some items might be animating up to close the gap left by the removed item
                    // this is not exclusive, both movement can be happening at the same time
                    // to reproduce this leave just enough items so the first one and the last one would be just a little off screen
                    // then remove one from the middle

                    // find first child with translationY > 0
                    // and last one with translationY < 0
                    // we're after a rect that is not covered in recycler-view views at this point in time
                    View lastViewComingDown = null;
                    View firstViewComingUp = null;

                    // this is fixed
                    int left = 0;
                    int right = parent.getWidth();

                    // this we need to find out
                    int top = 0;
                    int bottom = 0;

                    // find relevant translating views
                    int childCount = parent.getLayoutManager().getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View child = parent.getLayoutManager().getChildAt(i);
                        if (child.getTranslationY() < 0) {
                            // view is coming down
                            lastViewComingDown = child;
                        } else if (child.getTranslationY() > 0) {
                            // view is coming up
                            if (firstViewComingUp == null) {
                                firstViewComingUp = child;
                            }
                        }
                    }

                    if (lastViewComingDown != null && firstViewComingUp != null) {
                        // views are coming down AND going up to fill the void
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    } else if (lastViewComingDown != null) {
                        // views are going down to fill the void
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = lastViewComingDown.getBottom();
                    } else if (firstViewComingUp != null) {
                        // views are coming up to fill the void
                        top = firstViewComingUp.getTop();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    }

                    background.setBounds(left, top, right, bottom);
                    background.draw(c);

                }
                super.onDraw(c, parent, state);
            }

        });
    }


    private void restoreDetalles() {
        try {
            if (pedido != null) {
                if (pedido.getId() != null) {
                    DetallePedidoDao dao = new DetallePedidoDao(this);
                    if (adapter != null) {
                        if (detalles == null) {
                            detalles = new ArrayList<>();
                        }
                        detalles.clear();
                        detalles.addAll(dao.getByPedido(pedido));
                        adapter.notifyDataSetChanged();
                    } else {
                        detalles = dao.getByPedido(pedido);
                    }
                } else {
                    if (detalles == null) {
                        detalles = new ArrayList<>();
                        adapter = new DetallesAdapter(this, detalles);
                        adapter.setUndoOn(false);
                        detalleList.setAdapter(adapter);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void restoreDefaultPedido() {
        try {
            PedidoDao dao = new PedidoDao(this);
            pedido = dao.getDefaultPedido();
            if (pedido != null) {
                cliente = pedido.getCliente();
                total = pedido.getTotal();
                this.actualizarCampos();
            } else {
                SharedPreferences pref = this.getSharedPreferences(this.getString(R.string.global_prefs), MODE_PRIVATE);
                int usuario = pref.getInt(this.getString(R.string.global_prefs_id_user), 1);
                pedido = new Pedido();
                pedido.setEnviado(false);
                pedido.setTotal_lineas(0);
                pedido.setUsuario(new Usuario(usuario, null, null, null));
                pedido.setTotal(BigDecimal.ZERO);
                Calendar c = Calendar.getInstance();
                pedido.setFecha(new java.sql.Date(c.getTimeInMillis()));
                pedido.setHoraInicio(new java.sql.Timestamp(c.getTimeInMillis()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarCampos() {
        if ((pedido != null) && (cliente != null)) {
            TextView clienteField = (TextView) findViewById(R.id.cliente_field);
            TextView direccionField = (TextView) findViewById(R.id.direccion_field);
            TextView codigoField = (TextView) findViewById(R.id.codigo_field);
            TextView fechaField = (TextView) findViewById(R.id.fecha_field);
            TextView totalField = (TextView) findViewById(R.id.total_field);
            clienteField.setText(pedido.getCliente().getNombre());
            direccionField.setText(pedido.getCliente().getDireccion());
            codigoField.setText(pedido.getCliente().getCodigo() + " " + pedido.getCliente().getNit());
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String date = df.format(pedido.getFecha());
            fechaField.setText(date);
            Locale locale = Locale.getDefault();
            NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
            totalField.setText(numberFormat.format(pedido.getTotal()));
        }
    }

    public void actualizarTotal() {
        total = BigDecimal.ZERO;
        int count = 0;
        for (DetallePedido l : detalles) {
            BigDecimal add = l.getPrecio();
            total = total.add(add);
            count++;
        }
        TextView totalField = (TextView) findViewById(R.id.total_field);
        Locale locale = Locale.getDefault();
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        totalField.setText(numberFormat.format(total));
        pedido.setTotal(total);
        pedido.setTotal_lineas(count);
    }

    private void guardarPedido() {
        if (pedido != null) {
            if (pedido.getCliente() != null) {
                if (!detalles.isEmpty()) {
                    try {
                        PedidoDao dao = new PedidoDao(this);
                        DetallePedidoDao dao2 = new DetallePedidoDao(this);
                        Calendar c = Calendar.getInstance();
                        pedido.setHoraFinal(new java.sql.Timestamp(c.getTimeInMillis()));
                        if (pedido.getId() != null) {
                            dao.update(pedido);
                        } else {
                            pedido.setId(dao.insert(pedido));
                        }
                        for (DetallePedido l : detalles) {
                            if (l.getId() != null) {
                                dao2.update(l);
                            } else {
                                l.setId(dao2.insert(l));
                            }
                        }
                        nuevoPedido(false);
                        Snackbar.make(findViewById(R.id.activity_toma_pedidos), R.string.save_success, Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Snackbar.make(findViewById(R.id.activity_toma_pedidos), R.string.error_no_products, Snackbar.LENGTH_LONG).show();
                }
            } else {
                Snackbar.make(findViewById(R.id.activity_toma_pedidos), R.string.error_no_client, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private void guardarPedidoTemporal() {
        if (pedido != null) {
            try {
                PedidoDao dao = new PedidoDao(this);
                DetallePedidoDao dao2 = new DetallePedidoDao(this);
                pedido.setHoraFinal(null);
                if (pedido.getId() != null) {
                    dao.update(pedido);
                } else {
                    if (pedido.getCliente() != null) {
                        pedido.setId(dao.insert(pedido));
                    }
                }
                for (DetallePedido l : detalles) {
                    l.setPedido(pedido);
                    if (l.getId() != null) {
                        dao2.update(l);
                    } else {
                        l.setId(dao2.insert(l));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void limpiarPedido() {
        SharedPreferences pref = this.getSharedPreferences(this.getString(R.string.global_prefs), MODE_PRIVATE);
        int usuario = pref.getInt(this.getString(R.string.global_prefs_id_user), 1);
        if (pedido.getHoraFinal() == null) {
            DetallePedidoDao da1 = new DetallePedidoDao(this);
            PedidoDao da2 = new PedidoDao(this);
            pedido.setTotal(BigDecimal.ZERO);
            Calendar c = Calendar.getInstance();
            pedido.setFecha(new java.sql.Date(c.getTimeInMillis()));
            pedido.setHoraInicio(new java.sql.Timestamp(c.getTimeInMillis()));
            pedido.setTotal_lineas(0);
            total = BigDecimal.ZERO;
            try {
                da1.delete(detalles);
                if (pedido.getId() != null) {
                    da2.update(pedido);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            actualizarCampos();
        } else {
            cliente = null;
            pedido = new Pedido();
            pedido.setEnviado(false);
            pedido.setTotal_lineas(0);
            pedido.setUsuario(new Usuario(usuario, null, null, null));
            pedido.setTotal(BigDecimal.ZERO);
            total = BigDecimal.ZERO;
            Calendar c = Calendar.getInstance();
            pedido.setFecha(new java.sql.Date(c.getTimeInMillis()));
            pedido.setHoraInicio(new java.sql.Timestamp(c.getTimeInMillis()));
            TextView clienteField = (TextView) findViewById(R.id.cliente_field);
            TextView direccionField = (TextView) findViewById(R.id.direccion_field);
            TextView codigoField = (TextView) findViewById(R.id.codigo_field);
            TextView fechaField = (TextView) findViewById(R.id.fecha_field);
            TextView totalField = (TextView) findViewById(R.id.total_field);
            clienteField.setText("");
            direccionField.setText("");
            codigoField.setText("");
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String date = df.format(pedido.getFecha());
            fechaField.setText(date);
            Locale locale = Locale.getDefault();
            NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
            totalField.setText(numberFormat.format(BigDecimal.ZERO));
        }
        detalles.clear();
        adapter.notifyDataSetChanged();
    }

    private void nuevoPedido(boolean confirmacion) {
        if (confirmacion) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.confirm_new_title)
                    .setMessage(R.string.confirm_new_text)
                    .setPositiveButton(R.string.yes_value, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            pedido.setHoraFinal(null);
                            limpiarPedido();
                        }

                    })
                    .setNegativeButton(R.string.no_value, null)
                    .show();
        } else {
            limpiarPedido();
        }
    }

    private void buscarProducto() {
        Intent i = new Intent(this, ProductosActivity.class);
        i.putExtra(getString(R.string.selection_mode), true);
        if (cliente != null) {
            if (cliente.getListaPrecio() != null) {
                i.putExtra(getString(R.string.default_list), cliente.getListaPrecio().getId());
            }
        }
        startActivityForResult(i, PRODUCTO_SELECCION_CODE);
    }

    private void buscarCliente() {
        Intent i = new Intent(this, ClientesActivity.class);
        i.putExtra(getString(R.string.selection_mode), true);
        startActivityForResult(i, CLIENTE_SELECCION_CODE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        guardarPedidoTemporal();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        restoreDefaultPedido();
        restoreDetalles();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_guardar) {
            guardarPedido();
        } else if (id == R.id.action_nuevo) {
            nuevoPedido(true);
        } else if (id == R.id.action_busca_productos) {
            buscarProducto();
        } else if (id == R.id.action_busca_clientes) {
            buscarCliente();
        }
        VibratorUtil.vibrate(this, 50);


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        VibratorUtil.vibrate(this, 50);
        guardarPedidoTemporal();
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toma_pedidos, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        TextView label = (TextView) this.findViewById(R.id.cliente_label);
        TextView nombre = (TextView) this.findViewById(R.id.cliente_field);
        TextView codigo = (TextView) this.findViewById(R.id.codigo_field);
        TextView label2 = (TextView) this.findViewById(R.id.codigo_label);
        if ((v.getId() == label.getId()) ||
                (v.getId() == label2.getId()) ||
                (v.getId() == codigo.getId()) ||
                (v.getId() == nombre.getId())) {
            buscarCliente();
            VibratorUtil.vibrate(this, 50);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PRODUCTO_SELECCION_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                int producto = data.getIntExtra("PRODUCTO", 0);
                String precio = data.getStringExtra("PRECIO");
                int unidad = data.getIntExtra("UNIDAD", 0);
                if (precio != null) {
                    try {
                        ProductoDao dao = new ProductoDao(this);
                        UnidadDao dao2 = new UnidadDao(this);
                        Unidad uni = dao2.getById(Integer.valueOf(unidad));
                        Producto prod = dao.getById(Integer.valueOf(producto));
                        DetallePedido linea = new DetallePedido();
                        linea.setCantidad(BigDecimal.ONE);
                        linea.setProducto(prod);
                        linea.setUnidad(uni);
                        linea.setPrecio(new BigDecimal(precio));
                        linea.setPrecioUnitario(linea.getPrecio());
                        linea.setBonificacion(false);
                        linea.setPedido(pedido);
                        dao.subtractExistencias(prod, BigDecimal.ONE);
                        detalles.add(linea);
                        adapter.notifyDataSetChanged();
                        actualizarTotal();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (requestCode == CLIENTE_SELECCION_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                int cli = data.getIntExtra("CLIENTE", 0);
                if (cli != 0) {
                    try {
                        ClienteDao dao = new ClienteDao(this);
                        Cliente client = dao.getById(cli);
                        this.cliente = client;
                        if (pedido != null) {
                            pedido.setCliente(client);
                        }
                        Calendar c = Calendar.getInstance();
                        pedido.setHoraInicio(new java.sql.Timestamp(c.getTimeInMillis()));
                        pedido.setFecha(new java.sql.Date(c.getTimeInMillis()));
                        actualizarCampos();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
