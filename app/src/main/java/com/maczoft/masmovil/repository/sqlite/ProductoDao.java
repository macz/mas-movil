/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.Categoria;
import com.maczoft.masmovil.domain.Producto;
import com.maczoft.masmovil.repository.BaseDao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class ProductoDao extends MoonsideSQLiteHelper implements
        BaseDao<Producto, Integer> {

    public ProductoDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(Producto object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("_id", object.getId());
            nuevoRegistro.put("nombre", object.getNombre());
            nuevoRegistro.put("codigo", object.getCodigo());
            nuevoRegistro.put("marca", object.getMarca());
            nuevoRegistro.put("existencia", object.getExistencia().toString());
            nuevoRegistro.put("existencia_minima", object.getExistenciaMinima().toString());

            id = (int) this.getMyDataBase().insert("producto", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<Producto> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (Producto object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("_id", object.getId());
                nuevoRegistro.put("nombre", object.getNombre());
                nuevoRegistro.put("codigo", object.getCodigo());
                nuevoRegistro.put("marca", object.getMarca());
                nuevoRegistro.put("existencia", object.getExistencia()
                        .toString());
                nuevoRegistro.put("existencia_minima", object.getExistenciaMinima().toString());
                Integer id = (int) this.getMyDataBase().insert("producto",
                        null, nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro: " + e.getMessage());
        }
        return ids;
    }

    @Override
    public Integer update(Producto object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("nombre", object.getNombre());
            registro.put("codigo", object.getCodigo());
            registro.put("marca", object.getMarca());
            registro.put("existencia", object.getExistencia().toString());
            registro.put("existencia_minima", object.getExistenciaMinima().toString());
            modificados = this.getMyDataBase().update("producto", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<Producto> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (Producto object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("nombre", object.getNombre());
                registro.put("codigo", object.getCodigo());
                registro.put("marca", object.getMarca());
                registro.put("existencia", object.getExistencia().toString());
                registro.put("existencia_minima", object.getExistenciaMinima().toString());
                modificados += this.getMyDataBase().update("producto",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(Producto object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("producto",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<Producto> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (Producto object : objects) {
                borrados += this.getMyDataBase().delete("producto",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("producto",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<Producto> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Producto> registros = new ArrayList<Producto>();
        String query = "SELECT _id, nombre, marca, codigo, existencia, existencia_minima FROM producto ORDER BY nombre LIMIT 100";
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public Producto getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Producto> registros = new ArrayList<Producto>();
        String query = "SELECT _id, nombre, marca, codigo, existencia, existencia_minima FROM producto WHERE _id="
                + id;
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<Producto> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Producto> registros = new ArrayList<Producto>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<Producto> generateList(Cursor c) {
        List<Producto> registros = new ArrayList<Producto>();
        if (c.moveToFirst()) {
            do {
                Producto producto = new Producto();
                producto.setId(c.getInt(c.getColumnIndex("_id")));
                producto.setNombre(c.getString(c.getColumnIndex("nombre")));
                producto.setMarca(c.getString(c.getColumnIndex("marca")));
                producto.setCodigo(c.getString(c.getColumnIndex("codigo")));
                producto.setExistencia(new BigDecimal(c.getInt(c
                        .getColumnIndex("existencia"))));
                producto.setExistenciaMinima(new BigDecimal(c.getInt(c
                        .getColumnIndex("existencia_minima"))));
                Cursor e = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre FROM categoria c INNER JOIN producto_categoria p ON c._id=p.categoria WHERE p.producto=" + producto.getId(), null);
                if (e.moveToFirst()) {
                    List<Categoria> categorias = new ArrayList<Categoria>();
                    do {
                        Categoria cat = new Categoria();
                        cat.setId(e.getInt(e.getColumnIndex("_id")));
                        cat.setNombre(e.getString(e.getColumnIndex("nombre")));
                        categorias.add(cat);
                    } while (e.moveToNext());
                    producto.setCategorias(categorias);
                }
                registros.add(producto);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }

    public Producto getByNombre(String nombre) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Producto> registros = new ArrayList<Producto>();
        String query = "SELECT _id, nombre, marca, codigo, existencia, existencia_minima FROM producto WHERE nombre='"
                + nombre + "'";
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        c.close();
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    public Producto getByCodigo(String codigo) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Producto> registros = new ArrayList<Producto>();
        String query = "SELECT _id, nombre, marca, codigo, existencia, existencia_minima FROM producto WHERE codigo='"
                + codigo + "'";
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        c.close();
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    public BigDecimal getPrecio(int producto, int listaPrecio) {
        this.setMyDataBase(this.getReadableDatabase());
        String query = "SELECT precio FROM precio_producto WHERE producto="
                + producto + " AND lista=" + listaPrecio;
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        if (c.moveToFirst()) {
            BigDecimal precio = new BigDecimal(c.getString(c
                    .getColumnIndex("precio")));
            c.close();
            this.getReadableDatabase().close();
            return precio;
        } else {
            c.close();
            this.getReadableDatabase().close();
            return BigDecimal.ZERO;
        }
    }

    public Integer getUnidad(int producto, int listaPrecio) {
        this.setMyDataBase(this.getReadableDatabase());
        String query = "SELECT unidad FROM precio_producto WHERE producto="
                + producto + " AND lista=" + listaPrecio;
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        if (c.moveToFirst()) {
            Integer unidad = Integer.valueOf(c.getString(c.getColumnIndex("unidad")));
            c.close();
            this.getReadableDatabase().close();
            return unidad;
        } else {
            c.close();
            this.getReadableDatabase().close();
            return null;
        }
    }

    public String[] toStringList(List<Producto> productos) {
        String[] cadenas = new String[productos.size()];
        int i = 0;
        for (Producto p : productos) {
            cadenas[i] = p.getNombre();
            i++;
        }
        return cadenas;
    }

    public List<String> getByTarget(String target) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<String> registros = new ArrayList<String>();
        target = target.replace(" ", "%");
        String query = "SELECT nombre FROM producto WHERE codigo LIKE '%" + target + "%' OR nombre LIKE '%" + target + "%' LIMIT 10";
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                String cad = c.getString(c.getColumnIndex("nombre"));
                registros.add(cad);
            } while (c.moveToNext());
        }
        this.getReadableDatabase().close();
        return registros;
    }

    public List<Producto> getProductosByTarget(String target) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Producto> registros = new ArrayList<Producto>();
        target = target.replace(" ", "%");
        String query = "SELECT _id, nombre, marca, codigo, existencia, existencia_minima FROM producto WHERE codigo LIKE '%" + target + "%' OR nombre LIKE '%" + target + "%' ORDER BY nombre LIMIT 100";
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    public BigDecimal addExistencias(Producto producto, BigDecimal cantidad) throws Exception {
        if ((cantidad != null) && (producto != null)) {
            try {
                startTransaction();
                BigDecimal existenciaFinal = producto.getExistencia().add(cantidad);
                ContentValues registro = new ContentValues();
                registro.put("existencia", existenciaFinal.toString());
                this.getMyDataBase().update("producto",
                        registro, "_id=" + producto.getId(), null);
                commitTransaction();
                //Log.d("ADD_STOCK",producto.getNombre()+":  "+producto.getExistencia()+" + "+cantidad+" = "+existenciaFinal);
                producto.setExistencia(existenciaFinal);
                return existenciaFinal;
            } catch (Exception e) {
                rollbackTransanction();
                throw new Exception("No se pudo actualizar el registro");
            }
        }
        return null;
    }

    public BigDecimal subtractExistencias(Producto producto, BigDecimal cantidad) throws Exception {
        if ((cantidad != null) && (producto != null)) {
            try {
                startTransaction();
                BigDecimal existenciaFinal = producto.getExistencia().subtract(cantidad);
                ContentValues registro = new ContentValues();
                registro.put("existencia", existenciaFinal.toString());
                this.getMyDataBase().update("producto",
                        registro, "_id=" + producto.getId(), null);
                commitTransaction();
                //Log.d("SUBTRACT_STOCK",producto.getNombre()+":  "+producto.getExistencia()+" - "+cantidad+" = "+existenciaFinal);
                producto.setExistencia(existenciaFinal);
                return existenciaFinal;
            } catch (Exception e) {
                rollbackTransanction();
                throw new Exception("No se pudo actualizar el registro");
            }
        }
        return null;
    }
}
