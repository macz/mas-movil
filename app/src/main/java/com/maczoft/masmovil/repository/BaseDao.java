/**
 *
 */
package com.maczoft.masmovil.repository;

import java.io.Serializable;
import java.util.List;

/**
 * @author maczoe
 */
public interface BaseDao<T extends Serializable, E> {
    void startTransaction() throws Exception;

    void commitTransaction() throws Exception;

    void rollbackTransanction() throws Exception;

    E insert(T object) throws Exception;

    List<E> insert(List<T> objects) throws Exception;

    E update(T object) throws Exception;

    E update(List<T> object) throws Exception;

    E delete(T object) throws Exception;

    E delete(List<T> object) throws Exception;

    List<T> getAll() throws Exception;

    T getById(E id) throws Exception;

    List<T> executeQuery(String query) throws Exception;

    void executUpdate(String query) throws Exception;

    void truncate() throws Exception;
}
