/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.Oferta;
import com.maczoft.masmovil.domain.Producto;
import com.maczoft.masmovil.domain.ReglaOferta;
import com.maczoft.masmovil.repository.BaseDao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class ReglaOfertaDao extends MoonsideSQLiteHelper implements
        BaseDao<ReglaOferta, Integer> {

    public ReglaOfertaDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(ReglaOferta object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("oferta", object.getOferta().getId());
            nuevoRegistro.put("producto", object.getProducto().getId());
            nuevoRegistro.put("condicion", object.getCondicion());
            nuevoRegistro.put("valor", object.getValor());
            id = (int) this.getMyDataBase().insert("regla_oferta", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<ReglaOferta> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (ReglaOferta object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("oferta", object.getOferta().getId());
                nuevoRegistro.put("producto", object.getProducto().getId());
                nuevoRegistro.put("condicion", object.getCondicion());
                nuevoRegistro.put("valor", object.getValor());
                Integer id = (int) this.getMyDataBase().insert(
                        "regla_oferta", null, nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(ReglaOferta object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("oferta", object.getOferta().getId());
            registro.put("producto", object.getProducto().getId());
            registro.put("condicion", object.getCondicion());
            registro.put("valor", object.getValor());
            modificados = this.getMyDataBase().update("regla_oferta",
                    registro, "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<ReglaOferta> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (ReglaOferta object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("oferta", object.getOferta().getId());
                registro.put("producto", object.getProducto().getId());
                registro.put("condicion", object.getCondicion());
                registro.put("valor", object.getValor());
                modificados += this.getMyDataBase().update(
                        "regla_oferta", registro, "_id=" + object.getId(),
                        null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(ReglaOferta object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("regla_oferta",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<ReglaOferta> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (ReglaOferta object : objects) {
                borrados += this.getMyDataBase().delete("regla_oferta",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("regla_oferta",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<ReglaOferta> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<ReglaOferta> registros = new ArrayList<ReglaOferta>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT _id, condicion, valor, producto, oferta FROM regla_oferta", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public ReglaOferta getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<ReglaOferta> registros = new ArrayList<ReglaOferta>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, condicion, valor, producto, oferta FROM regla_oferta WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<ReglaOferta> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<ReglaOferta> registros = new ArrayList<ReglaOferta>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<ReglaOferta> generateList(Cursor c) {
        List<ReglaOferta> registros = new ArrayList<ReglaOferta>();
        if (c.moveToFirst()) {
            do {
                ReglaOferta regla = new ReglaOferta();
                regla.setId(c.getInt(c.getColumnIndex("_id")));
                regla.setCondicion(c.getString(c
                        .getColumnIndex("condicion")));
                regla.setValor(c.getString(c
                        .getColumnIndex("valor")));

                Cursor d = this.getMyDataBase().rawQuery(
                        "SELECT  _id, nombre, descripcion, precio FROM oferta WHERE _id="
                                + c.getInt(c.getColumnIndex("oferta")), null);
                if (d.moveToFirst()) {
                    Oferta oferta = new Oferta();
                    oferta.setId(d.getInt(d.getColumnIndex("_id")));
                    oferta.setNombre(d.getString(d
                            .getColumnIndex("nombre")));
                    oferta.setDescripcion(d.getString(d
                            .getColumnIndex("descripcion")));
                    oferta.setPrecio(new BigDecimal(d.getString(d.getColumnIndex("precio"))));
                    regla.setOferta(oferta);
                }

                d = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre, marca, codigo, existencia FROM producto WHERE _id="
                                + c.getInt(c.getColumnIndex("producto")), null);
                if (d.moveToFirst()) {
                    Producto producto = new Producto();
                    producto.setId(d.getInt(d.getColumnIndex("_id")));
                    producto.setNombre(d.getString(d.getColumnIndex("nombre")));
                    producto.setMarca(d.getString(d.getColumnIndex("marca")));
                    producto.setExistencia(new BigDecimal(d.getString(d
                            .getColumnIndex("existencia"))));
                    regla.setProducto(producto);
                }
                d.close();
                registros.add(regla);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }
}
