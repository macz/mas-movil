/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.TipoCliente;
import com.maczoft.masmovil.repository.BaseDao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class TipoClienteDao extends MoonsideSQLiteHelper implements
        BaseDao<TipoCliente, Integer> {

    public TipoClienteDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(TipoCliente object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("nombre", object.getNombre());
            nuevoRegistro.put("_id", object.getId());
            id = (int) this.getMyDataBase().insert("tipo_cliente", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<TipoCliente> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (TipoCliente object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("nombre", object.getNombre());
                nuevoRegistro.put("_id", object.getId());
                Integer id = (int) this.getMyDataBase().insert("tipo_cliente", null,
                        nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(TipoCliente object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("nombre", object.getNombre());
            modificados = this.getMyDataBase().update("tipo_cliente", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<TipoCliente> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (TipoCliente object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("nombre", object.getNombre());
                modificados += this.getMyDataBase().update("tipo_cliente",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(TipoCliente object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("tipo_cliente",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<TipoCliente> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (TipoCliente object : objects) {
                borrados += this.getMyDataBase().delete("tipo_cliente",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("tipo_cliente",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<TipoCliente> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<TipoCliente> registros = new ArrayList<TipoCliente>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT _id, nombre FROM tipo_cliente", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public TipoCliente getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<TipoCliente> registros = new ArrayList<TipoCliente>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, nombre FROM tipo_cliente WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<TipoCliente> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<TipoCliente> registros = new ArrayList<TipoCliente>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<TipoCliente> generateList(Cursor c) {
        List<TipoCliente> registros = new ArrayList<TipoCliente>();
        if (c.moveToFirst()) {
            do {
                TipoCliente tipo = new TipoCliente();
                tipo.setId(c.getInt(c.getColumnIndex("_id")));
                tipo.setNombre(c.getString(c.getColumnIndex("nombre")));
                registros.add(tipo);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }
}
