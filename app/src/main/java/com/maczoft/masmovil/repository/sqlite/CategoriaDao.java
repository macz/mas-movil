/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.Categoria;
import com.maczoft.masmovil.repository.BaseDao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class CategoriaDao extends MoonsideSQLiteHelper implements
        BaseDao<Categoria, Integer> {

    public CategoriaDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(Categoria object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("_id", object.getId());
            nuevoRegistro.put("nombre", object.getNombre());
            id = (int) this.getMyDataBase().insert("categoria", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<Categoria> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (Categoria object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("_id", object.getId());
                nuevoRegistro.put("nombre", object.getNombre());
                Integer id = (int) this.getMyDataBase().insert("categoria", null,
                        nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(Categoria object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("nombre", object.getNombre());
            modificados = this.getMyDataBase().update("categoria", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<Categoria> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (Categoria object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("nombre", object.getNombre());
                modificados += this.getMyDataBase().update("categoria",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(Categoria object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("categoria",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<Categoria> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (Categoria object : objects) {
                borrados += this.getMyDataBase().delete("categoria",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("categoria",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<Categoria> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Categoria> registros = new ArrayList<Categoria>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT _id, nombre FROM categoria", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public Categoria getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Categoria> registros = new ArrayList<Categoria>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, nombre FROM categoria WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<Categoria> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Categoria> registros = new ArrayList<Categoria>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<Categoria> generateList(Cursor c) {
        List<Categoria> registros = new ArrayList<Categoria>();
        if (c.moveToFirst()) {
            do {
                Categoria categoria = new Categoria();
                categoria.setId(c.getInt(c.getColumnIndex("_id")));
                categoria.setNombre(c.getString(c.getColumnIndex("nombre")));
                registros.add(categoria);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }
}
