/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.ListaPrecio;
import com.maczoft.masmovil.repository.BaseDao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class ListaPrecioDao extends MoonsideSQLiteHelper implements
        BaseDao<ListaPrecio, Integer> {

    public ListaPrecioDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(ListaPrecio object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("_id", object.getId());
            nuevoRegistro.put("nombre", object.getNombre());
            id = (int) this.getMyDataBase().insert("lista_precio", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<ListaPrecio> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (ListaPrecio object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("_id", object.getId());
                nuevoRegistro.put("nombre", object.getNombre());
                Integer id = (int) this.getMyDataBase().insert("lista_precio", null,
                        nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(ListaPrecio object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("nombre", object.getNombre());
            modificados = this.getMyDataBase().update("lista_precio", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<ListaPrecio> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (ListaPrecio object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("nombre", object.getNombre());
                modificados += this.getMyDataBase().update("lista_precio",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(ListaPrecio object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("lista_precio",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<ListaPrecio> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (ListaPrecio object : objects) {
                borrados += this.getMyDataBase().delete("lista_precio",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("lista_precio",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<ListaPrecio> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<ListaPrecio> registros = new ArrayList<ListaPrecio>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT _id, nombre FROM lista_precio", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public ListaPrecio getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<ListaPrecio> registros = new ArrayList<ListaPrecio>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, nombre FROM lista_precio WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<ListaPrecio> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<ListaPrecio> registros = new ArrayList<ListaPrecio>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<ListaPrecio> generateList(Cursor c) {
        List<ListaPrecio> registros = new ArrayList<ListaPrecio>();
        if (c.moveToFirst()) {
            do {
                ListaPrecio lista = new ListaPrecio();
                lista.setId(c.getInt(c.getColumnIndex("_id")));
                lista.setNombre(c.getString(c.getColumnIndex("nombre")));
                registros.add(lista);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }

    public ListaPrecio getDefault() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<ListaPrecio> registros = new ArrayList<ListaPrecio>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, nombre FROM lista_precio", null);
        registros = generateList(c);

        this.getReadableDatabase().close();
        if (!registros.isEmpty()) {
            return registros.get(0);
        } else {
            return null;
        }
    }
}
