/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.Bodega;
import com.maczoft.masmovil.domain.Oferta;
import com.maczoft.masmovil.repository.BaseDao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class OfertaDao extends MoonsideSQLiteHelper implements
        BaseDao<Oferta, Integer> {

    public OfertaDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(Oferta object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("_id", object.getId());
            nuevoRegistro.put("nombre", object.getNombre());
            nuevoRegistro.put("descripcion", object.getDescripcion());
            nuevoRegistro.put("precio", object.getPrecio().toString());
            nuevoRegistro.put("bodega", object.getBodega().getId());
            id = (int) this.getMyDataBase().insert("oferta", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<Oferta> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (Oferta object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("_id", object.getId());
                nuevoRegistro.put("nombre", object.getNombre());
                nuevoRegistro.put("descripcion", object.getDescripcion());
                nuevoRegistro.put("precio", object.getPrecio().toString());
                nuevoRegistro.put("bodega", object.getBodega().getId());
                Integer id = (int) this.getMyDataBase().insert("oferta", null,
                        nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(Oferta object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("nombre", object.getNombre());
            registro.put("descripcion", object.getDescripcion());
            registro.put("precio", object.getPrecio().toString());
            registro.put("bodega", object.getBodega().getId());
            modificados = this.getMyDataBase().update("oferta", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<Oferta> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (Oferta object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("nombre", object.getNombre());
                registro.put("descripcion", object.getDescripcion());
                registro.put("precio", object.getPrecio().toString());
                registro.put("bodega", object.getBodega().getId());
                modificados += this.getMyDataBase().update("oferta",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(Oferta object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("oferta",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<Oferta> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (Oferta object : objects) {
                borrados += this.getMyDataBase().delete("oferta",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("oferta",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<Oferta> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Oferta> registros = new ArrayList<Oferta>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT _id, nombre, descripcion, precio, bodega FROM oferta", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public Oferta getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Oferta> registros = new ArrayList<Oferta>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, nombre, descripcion, precio, bodega FROM oferta WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<Oferta> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Oferta> registros = new ArrayList<Oferta>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<Oferta> generateList(Cursor c) {
        List<Oferta> registros = new ArrayList<Oferta>();
        if (c.moveToFirst()) {
            do {
                Oferta oferta = new Oferta();
                oferta.setId(c.getInt(c.getColumnIndex("_id")));
                oferta.setNombre(c.getString(c.getColumnIndex("nombre")));
                oferta.setDescripcion(c.getString(c.getColumnIndex("descripcion")));
                oferta.setPrecio(new BigDecimal(c.getString(c.getColumnIndex("precio"))));
                Cursor f = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre FROM bodega WHERE _id="
                                + c.getInt(c.getColumnIndex("bodega")),
                        null);
                if (f.moveToFirst()) {
                    Bodega bodega = new Bodega();
                    bodega.setId(f.getInt(f.getColumnIndex("_id")));
                    bodega.setNombre(f.getString(f.getColumnIndex("nombre")));
                    oferta.setBodega(bodega);
                }
                registros.add(oferta);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }

    public List<Oferta> getByBodega(Integer bodega) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Oferta> registros = new ArrayList<Oferta>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, nombre, descripcion, precio, bodega FROM oferta WHERE bodega=" + bodega, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }
}
