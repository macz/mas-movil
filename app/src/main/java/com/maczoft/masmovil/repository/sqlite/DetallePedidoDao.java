/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.maczoft.masmovil.domain.DetallePedido;
import com.maczoft.masmovil.domain.Pedido;
import com.maczoft.masmovil.domain.Producto;
import com.maczoft.masmovil.domain.Unidad;
import com.maczoft.masmovil.repository.BaseDao;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author maczoe
 */
public class DetallePedidoDao extends MoonsideSQLiteHelper implements
        BaseDao<DetallePedido, Integer> {

    public DetallePedidoDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(DetallePedido object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("pedido", object.getPedido().getId());
            nuevoRegistro.put("producto", object.getProducto().getId());
            nuevoRegistro.put("cantidad", object.getCantidad().toString());
            nuevoRegistro.put("precio", object.getPrecio().toString());
            nuevoRegistro.put("unidad", object.getUnidad().getId());
            nuevoRegistro.put("bonificacion", object.isBonificacion());
            id = (int) this.getMyDataBase().insert("detalle_pedido", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<DetallePedido> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (DetallePedido object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("pedido", object.getPedido().getId());
                nuevoRegistro.put("producto", object.getProducto().getId());
                nuevoRegistro.put("cantidad", object.getCantidad().toString());
                nuevoRegistro.put("precio", object.getPrecio().toString());
                nuevoRegistro.put("unidad", object.getUnidad().getId());
                nuevoRegistro.put("bonificacion", object.isBonificacion());
                nuevoRegistro.put("bonificacion", object.isBonificacion());
                Integer id = (int) this.getMyDataBase().insert(
                        "detalle_pedido", null, nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(DetallePedido object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("pedido", object.getPedido().getId());
            registro.put("producto", object.getProducto().getId());
            registro.put("cantidad", object.getCantidad().toString());
            registro.put("precio", object.getPrecio().toString());
            registro.put("unidad", object.getUnidad().getId());
            registro.put("bonificacion", object.isBonificacion());
            modificados = this.getMyDataBase().update("detalle_pedido",
                    registro, "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<DetallePedido> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (DetallePedido object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("pedido", object.getPedido().getId());
                registro.put("producto", object.getProducto().getId());
                registro.put("cantidad", object.getCantidad().toString());
                registro.put("precio", object.getPrecio().toString());
                registro.put("unidad", object.getUnidad().getId());
                registro.put("bonificacion", object.isBonificacion());
                modificados += this.getMyDataBase().update(
                        "detalle_pedido", registro, "_id=" + object.getId(),
                        null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(DetallePedido object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("detalle_pedido",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<DetallePedido> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (DetallePedido object : objects) {
                borrados += this.getMyDataBase().delete("detalle_pedido",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("detalle_pedido",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<DetallePedido> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<DetallePedido> registros = new ArrayList<DetallePedido>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT _id, pedido, producto, cantidad, precio, unidad FROM detalle_pedido", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public DetallePedido getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<DetallePedido> registros = new ArrayList<DetallePedido>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, pedido, producto, cantidad, precio, unidad FROM detalle_pedido WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<DetallePedido> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<DetallePedido> registros = new ArrayList<DetallePedido>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<DetallePedido> generateList(Cursor c) {
        List<DetallePedido> registros = new ArrayList<DetallePedido>();
        if (c.moveToFirst()) {
            do {
                DetallePedido detalle = new DetallePedido();
                detalle.setId(c.getInt(c.getColumnIndex("_id")));
                detalle.setCantidad(new BigDecimal(c.getString(c
                        .getColumnIndex("cantidad"))));
                detalle.setPrecio(new BigDecimal(c.getString(c
                        .getColumnIndex("precio"))));
                detalle.setPrecioUnitario(detalle.getPrecio().divide(detalle.getCantidad(), 4, BigDecimal.ROUND_UP));

                Cursor d = this.getMyDataBase().rawQuery(
                        "SELECT _id, fecha, cliente, hora_inicio, hora_final, total, observaciones, enviado, usuario FROM pedido WHERE _id="
                                + c.getInt(c.getColumnIndex("pedido")), null);
                if (d.moveToFirst()) {
                    Pedido pedido = new Pedido();
                    pedido.setId(d.getInt(d.getColumnIndex("_id")));
                    pedido.setObservaciones(d.getString(d
                            .getColumnIndex("observaciones")));
                    pedido.setTotal(new BigDecimal(d.getString(d
                            .getColumnIndex("total"))));
                    pedido.setEnviado(d.getInt(d.getColumnIndex("enviado")) > 0);
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyy-MM-dd", Locale.getDefault());
                    long time = new java.util.Date().getTime();
                    try {
                        time = dateFormat.parse(
                                d.getString(d.getColumnIndex("fecha")))
                                .getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Date fecha = new Date(time);
                    pedido.setFecha(fecha);
                    dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
                            Locale.getDefault());
                    java.util.Date date = null;
                    try {
                        date = dateFormat.parse(d.getString(d
                                .getColumnIndex("hora_inicio")));
                    } catch (ParseException e) {
                        Log.d("Error DetallePedidoDao", e.getLocalizedMessage());
                        e.printStackTrace();
                    }
                    java.sql.Timestamp inicio = new java.sql.Timestamp(
                            date.getTime());
                    pedido.setHoraInicio(inicio);
                    try {
                        if (d.getString(d.getColumnIndex("hora_final")) != null) {
                            date = dateFormat.parse(d.getString(d
                                    .getColumnIndex("hora_final")));
                        }
                    } catch (ParseException e) {
                        Log.d("Error PedidoDao", e.getLocalizedMessage());
                        e.printStackTrace();
                    }
                    java.sql.Timestamp fin = new java.sql.Timestamp(
                            date.getTime());
                    pedido.setHoraFinal(fin);
                    detalle.setPedido(pedido);
                }

                d = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre, codigo, marca, existencia FROM producto WHERE _id="
                                + c.getInt(c.getColumnIndex("producto")), null);
                if (d.moveToFirst()) {
                    Producto producto = new Producto();
                    producto.setId(d.getInt(d.getColumnIndex("_id")));
                    producto.setNombre(d.getString(d.getColumnIndex("nombre")));
                    producto.setMarca(d.getString(d.getColumnIndex("marca")));
                    producto.setExistencia(new BigDecimal(d.getString(d
                            .getColumnIndex("existencia"))));
                    detalle.setProducto(producto);
                }

                d = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre, abreviatura, ubase FROM unidad WHERE _id="
                                + c.getInt(c.getColumnIndex("unidad")), null);
                if (d.moveToFirst()) {
                    Unidad unidad = new Unidad();
                    unidad.setId(d.getInt(d.getColumnIndex("_id")));
                    unidad.setNombre(d.getString(d.getColumnIndex("nombre")));
                    unidad.setAbreviatura(d.getString(d
                            .getColumnIndex("abreviatura")));
                    unidad.setUbase(new BigDecimal(d.getString(d
                            .getColumnIndex("ubase"))));
                    detalle.setUnidad(unidad);
                }
                d.close();
                registros.add(detalle);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }


    public List<DetallePedido> getByPedido(Pedido pedido) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<DetallePedido> registros;
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, pedido, producto, cantidad, precio, unidad FROM detalle_pedido WHERE pedido=" + pedido.getId(), null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }
}
