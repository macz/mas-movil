/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.maczoft.masmovil.domain.Bodega;
import com.maczoft.masmovil.domain.Cliente;
import com.maczoft.masmovil.domain.Pedido;
import com.maczoft.masmovil.domain.Usuario;
import com.maczoft.masmovil.repository.BaseDao;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author maczoe
 */
public class PedidoDao extends MoonsideSQLiteHelper implements
        BaseDao<Pedido, Integer> {

    public PedidoDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(Pedido object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("fecha", object.getFecha().toString());
            nuevoRegistro.put("cliente", object.getCliente().getId());
            nuevoRegistro.put("hora_inicio", object.getHoraInicio().toString());
            nuevoRegistro.put("total", object.getTotal().toString());
            nuevoRegistro.put("observaciones", object.getObservaciones());
            nuevoRegistro.put("enviado", object.getEnviado());
            nuevoRegistro.put("usuario", object.getUsuario().getId());
            nuevoRegistro.put("total_lineas", object.getTotal_lineas());
            if (object.getHoraFinal() != null)
                nuevoRegistro.put("hora_final", object.getHoraFinal().toString());

            id = (int) this.getMyDataBase().insert("pedido", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<Pedido> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (Pedido object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("fecha", object.getFecha().toString());
                nuevoRegistro.put("cliente", object.getCliente().getId());
                nuevoRegistro.put("hora_inicio", object.getHoraInicio().toString());
                nuevoRegistro.put("total", object.getTotal().toString());
                nuevoRegistro.put("observaciones", object.getObservaciones());
                nuevoRegistro.put("enviado", object.getEnviado());
                nuevoRegistro.put("usuario", object.getUsuario().getId());
                nuevoRegistro.put("total_lineas", object.getTotal_lineas());
                if (object.getHoraFinal() != null)
                    nuevoRegistro.put("hora_final", object.getHoraFinal().toString());
                Integer id = (int) this.getMyDataBase().insert("pedido", null,
                        nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(Pedido object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("fecha", object.getFecha().toString());
            registro.put("cliente", object.getCliente().getId());
            registro.put("hora_inicio", object.getHoraInicio().toString());
            registro.put("total", object.getTotal().toString());
            registro.put("observaciones", object.getObservaciones());
            registro.put("enviado", object.getEnviado());
            registro.put("usuario", object.getUsuario().getId());
            registro.put("total_lineas", object.getTotal_lineas());
            if (object.getHoraFinal() != null)
                registro.put("hora_final", object.getHoraFinal().toString());
            else
                registro.putNull("hora_final");
            modificados = this.getMyDataBase().update("pedido", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<Pedido> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (Pedido object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("fecha", object.getFecha().toString());
                registro.put("cliente", object.getCliente().getId());
                registro.put("hora_inicio", object.getHoraInicio().toString());
                registro.put("total", object.getTotal().toString());
                registro.put("observaciones", object.getObservaciones());
                registro.put("enviado", object.getEnviado());
                registro.put("usuario", object.getUsuario().getId());
                registro.put("total_lineas", object.getTotal_lineas());
                if (object.getHoraFinal() != null)
                    registro.put("hora_final", object.getHoraFinal().toString());
                modificados += this.getMyDataBase().update("pedido",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(Pedido object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("pedido",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<Pedido> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (Pedido object : objects) {
                borrados += this.getMyDataBase().delete("pedido",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("pedido",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<Pedido> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Pedido> registros = new ArrayList<Pedido>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT _id, fecha, cliente, hora_inicio, hora_final, total, observaciones, enviado, usuario, total_lineas FROM pedido", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public Pedido getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Pedido> registros = new ArrayList<Pedido>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, fecha, cliente, hora_inicio, hora_final, total, observaciones, enviado, usuario, total_lineas FROM pedido WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }


    public List<Pedido> getPorEnviar(Integer usuario) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Pedido> registros = new ArrayList<Pedido>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, fecha, cliente, hora_inicio, hora_final, total, observaciones, enviado, usuario, total_lineas FROM pedido WHERE enviado=0 AND usuario=" + usuario + " AND hora_final NOT NULL", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public List<Pedido> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Pedido> registros = new ArrayList<Pedido>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<Pedido> generateList(Cursor c) {
        List<Pedido> registros = new ArrayList<Pedido>();
        if (c.moveToFirst()) {
            do {
                Pedido pedido = new Pedido();
                pedido.setId(c.getInt(c.getColumnIndex("_id")));
                pedido.setObservaciones(c.getString(c.getColumnIndex("observaciones")));
                pedido.setTotal(new BigDecimal(c.getString(c.getColumnIndex("total"))));
                pedido.setTotal_lineas(Integer.valueOf(c.getString(c.getColumnIndex("total_lineas"))));
                pedido.setEnviado(c.getInt(c.getColumnIndex("enviado")) > 0);
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "yyyy-MM-dd", Locale.getDefault());
                long time = new java.util.Date().getTime();
                try {
                    time = dateFormat.parse(
                            c.getString(c.getColumnIndex("fecha"))).getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Date fecha = new Date(time);
                pedido.setFecha(fecha);

                dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
                        Locale.getDefault());
                java.util.Date date = null;
                try {
                    date = dateFormat.parse(c.getString(c
                            .getColumnIndex("hora_inicio")));
                } catch (ParseException e) {
                    Log.d("Error PedidoDao", e.getLocalizedMessage());
                    e.printStackTrace();
                }
                java.sql.Timestamp inicio = new java.sql.Timestamp(
                        date.getTime());
                pedido.setHoraInicio(inicio);
                if (c.getString(c.getColumnIndex("hora_final")) != null) {
                    try {
                        date = dateFormat.parse(c.getString(c
                                .getColumnIndex("hora_final")));
                    } catch (ParseException e) {
                        Log.d("Error PedidoDao", e.getLocalizedMessage());
                        e.printStackTrace();
                    }
                    java.sql.Timestamp fin = new java.sql.Timestamp(
                            date.getTime());
                    pedido.setHoraFinal(fin);
                }
                Cursor e = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre, contrasena, bodega, comision FROM usuario WHERE _id="
                                + c.getInt(c.getColumnIndex("usuario")), null);
                if (e.moveToFirst()) {
                    Usuario usuario = new Usuario(e.getInt(e
                            .getColumnIndex("_id")), e.getString(e
                            .getColumnIndex("nombre")), e.getString(e
                            .getColumnIndex("contrasena")), new BigDecimal(
                            e.getString(e.getColumnIndex("comision"))));
                    Cursor f = this.getMyDataBase().rawQuery(
                            "SELECT _id, nombre FROM bodega WHERE _id="
                                    + e.getInt(e.getColumnIndex("bodega")),
                            null);
                    if (f.moveToFirst()) {
                        Bodega bodega = new Bodega();
                        bodega.setId(f.getInt(f.getColumnIndex("_id")));
                        bodega.setNombre(f.getString(f.getColumnIndex("nombre")));
                        usuario.setBodega(bodega);
                    }
                    pedido.setUsuario(usuario);
                    f.close();
                }
                if (c.getString(c.getColumnIndex("cliente")) != null) {
                    e = this.getMyDataBase().rawQuery(
                            "SELECT _id, nombre, codigo, nit, direccion FROM cliente WHERE _id="
                                    + c.getInt(c.getColumnIndex("cliente")), null);
                    if (e.moveToFirst()) {
                        Cliente cliente = new Cliente();
                        cliente.setId(e.getInt(e.getColumnIndex("_id")));
                        cliente.setNit(e.getString(e.getColumnIndex("nit")));
                        cliente.setNombre(e.getString(e.getColumnIndex("nombre")));
                        cliente.setDireccion(e.getString(e.getColumnIndex("direccion")));
                        cliente.setCodigo(e.getString(e.getColumnIndex("codigo")));
                        pedido.setCliente(cliente);
                    }
                    e.close();
                }
                registros.add(pedido);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }

    private Pedido singleResult(Cursor c) {
        if (c.moveToFirst()) {
            Pedido pedido = new Pedido();
            pedido.setId(c.getInt(c.getColumnIndex("_id")));
            pedido.setObservaciones(c.getString(c.getColumnIndex("observaciones")));
            pedido.setTotal(new BigDecimal(c.getString(c.getColumnIndex("total"))));
            pedido.setTotal_lineas(Integer.valueOf(c.getString(c.getColumnIndex("total_lineas"))));
            pedido.setEnviado(c.getInt(c.getColumnIndex("enviado")) > 0);
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd", Locale.getDefault());
            long time = new java.util.Date().getTime();
            try {
                time = dateFormat.parse(
                        c.getString(c.getColumnIndex("fecha"))).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date fecha = new Date(time);
            pedido.setFecha(fecha);

            dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
                    Locale.getDefault());
            java.util.Date date = null;
            try {
                date = dateFormat.parse(c.getString(c
                        .getColumnIndex("hora_inicio")));
            } catch (ParseException e) {
                Log.d("Error PedidoDao", e.getLocalizedMessage());
                e.printStackTrace();
            }
            java.sql.Timestamp inicio = new java.sql.Timestamp(
                    date.getTime());
            pedido.setHoraInicio(inicio);
            try {
                if (c.getString(c.getColumnIndex("hora_final")) != null) {
                    date = dateFormat.parse(c.getString(c
                            .getColumnIndex("hora_final")));
                }
            } catch (ParseException e) {
                Log.d("Error PedidoDao", e.getLocalizedMessage());
                e.printStackTrace();
            }
            java.sql.Timestamp fin = new java.sql.Timestamp(
                    date.getTime());
            pedido.setHoraFinal(fin);
            Cursor e = this.getMyDataBase().rawQuery(
                    "SELECT _id, nombre, contrasena, bodega, comision FROM usuario WHERE _id="
                            + c.getInt(c.getColumnIndex("usuario")), null);
            if (e.moveToFirst()) {
                Usuario usuario = new Usuario(e.getInt(e
                        .getColumnIndex("_id")), e.getString(e
                        .getColumnIndex("nombre")), e.getString(e
                        .getColumnIndex("contrasena")), new BigDecimal(
                        e.getString(e.getColumnIndex("comision"))));
                Cursor f = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre FROM bodega WHERE _id="
                                + e.getInt(e.getColumnIndex("bodega")),
                        null);
                if (f.moveToFirst()) {
                    Bodega bodega = new Bodega();
                    bodega.setId(f.getInt(f.getColumnIndex("_id")));
                    bodega.setNombre(f.getString(f.getColumnIndex("nombre")));
                    usuario.setBodega(bodega);
                }
                pedido.setUsuario(usuario);
                f.close();
            }
            e = this.getMyDataBase().rawQuery(
                    "SELECT _id, nombre, codigo, nit, direccion FROM cliente WHERE _id="
                            + c.getInt(c.getColumnIndex("cliente")), null);
            if (e.moveToFirst()) {
                Cliente cliente = new Cliente();
                cliente.setId(e.getInt(e.getColumnIndex("_id")));
                cliente.setNit(e.getString(e.getColumnIndex("nit")));
                cliente.setNombre(e.getString(e.getColumnIndex("nombre")));
                cliente.setDireccion(e.getString(e.getColumnIndex("direccion")));
                cliente.setCodigo(e.getString(e.getColumnIndex("codigo")));
                pedido.setCliente(cliente);
            }
            e.close();
            c.close();
            return pedido;
        } else {
            c.close();
            return null;
        }
    }

    public Pedido getDefaultPedido() throws Exception {
        this.setMyDataBase(getReadableDatabase());
        String query = "SELECT _id, fecha, cliente, hora_inicio, hora_final, total, observaciones, enviado, usuario, total_lineas FROM pedido WHERE hora_final IS NULL";
        Cursor p = this.getMyDataBase().rawQuery(query, null);
        Pedido pedido = this.singleResult(p);
        this.close();
        return pedido;
    }

    public List<Pedido> getPedidosByFilters(Date del, Date al, Boolean enviados) throws Exception {
        String query = "SELECT _id, fecha, cliente, hora_inicio, hora_final, total, observaciones, enviado, usuario, total_lineas FROM pedido WHERE hora_final IS NOT NULL AND cliente IS NOT NULL";
        return this.executeQuery(query);
    }
}
