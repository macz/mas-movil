/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.Bodega;
import com.maczoft.masmovil.domain.Usuario;
import com.maczoft.masmovil.repository.BaseDao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class UsuarioDao extends MoonsideSQLiteHelper implements
        BaseDao<Usuario, Integer> {

    public UsuarioDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(Usuario object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("_id", object.getId());
            nuevoRegistro.put("nombre", object.getNombre());
            nuevoRegistro.put("contrasena", object.getContrasena());
            nuevoRegistro.put("bodega", object.getBodega().getId());
            nuevoRegistro.put("comision", object.getComision().toString());
            nuevoRegistro.put("vendedor", object.getVendedor());
            id = (int) this.getMyDataBase().insert("usuario", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<Usuario> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (Usuario object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("_id", object.getId());
                nuevoRegistro.put("nombre", object.getNombre());
                nuevoRegistro.put("contrasena", object.getContrasena());
                nuevoRegistro.put("bodega", object.getBodega().getId());
                nuevoRegistro.put("comision", object.getComision().toString());
                nuevoRegistro.put("vendedor", object.getVendedor());
                Integer id = (int) this.getMyDataBase().insert("usuario", null,
                        nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(Usuario object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("nombre", object.getNombre());
            registro.put("contrasena", object.getContrasena());
            registro.put("bodega", object.getBodega().getId());
            registro.put("comision", object.getComision().toString());
            registro.put("vendedor", object.getVendedor());
            modificados = this.getMyDataBase().update("usuario", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<Usuario> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (Usuario object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("nombre", object.getNombre());
                registro.put("contrasena", object.getContrasena());
                registro.put("bodega", object.getBodega().getId());
                registro.put("comision", object.getComision().toString());
                registro.put("vendedor", object.getVendedor());
                modificados += this.getMyDataBase().update("usuario",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(Usuario object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("usuario",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<Usuario> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (Usuario object : objects) {
                borrados += this.getMyDataBase().delete("usuario",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("usuario",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<Usuario> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Usuario> registros = new ArrayList<Usuario>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT _id, nombre, contrasena, bodega, comision, vendedor FROM usuario", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public Usuario getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Usuario> registros = new ArrayList<Usuario>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, nombre, contrasena, bodega, comision, vendedor FROM usuario WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }


    public Usuario getByUsuarioContrasena(String user, String md5Pass) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Usuario> registros = new ArrayList<Usuario>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, nombre, contrasena, bodega, comision, vendedor FROM usuario WHERE UPPER(nombre)=UPPER('" + user + "') AND contrasena='" + md5Pass + "'", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (!registros.isEmpty())
            return registros.get(0);
        else
            return null;
    }

    @Override
    public List<Usuario> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Usuario> registros = new ArrayList<Usuario>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<Usuario> generateList(Cursor c) {
        List<Usuario> registros = new ArrayList<Usuario>();
        if (c.moveToFirst()) {
            do {
                Usuario usuario = new Usuario();
                usuario.setId(c.getInt(c.getColumnIndex("_id")));
                usuario.setNombre(c.getString(c.getColumnIndex("nombre")));
                usuario.setContrasena(c.getString(c.getColumnIndex("contrasena")));
                usuario.setVendedor(Integer.valueOf(c.getString(c.getColumnIndex("vendedor"))));
                usuario.setComision(new BigDecimal(c.getString(c
                        .getColumnIndex("comision"))));
                Cursor d = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre FROM bodega WHERE _id="
                                + c.getInt(c.getColumnIndex("bodega")), null);
                if (d.moveToFirst()) {
                    Bodega bodega = new Bodega();
                    bodega.setId(d.getInt(d.getColumnIndex("_id")));
                    bodega.setNombre(d.getString(d.getColumnIndex("nombre")));
                    usuario.setBodega(bodega);
                }
                d.close();
                registros.add(usuario);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }
}
