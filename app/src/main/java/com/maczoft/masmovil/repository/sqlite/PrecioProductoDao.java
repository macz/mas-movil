/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.ListaPrecio;
import com.maczoft.masmovil.domain.PrecioProducto;
import com.maczoft.masmovil.domain.Producto;
import com.maczoft.masmovil.domain.Unidad;
import com.maczoft.masmovil.repository.BaseDao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class PrecioProductoDao extends MoonsideSQLiteHelper implements
        BaseDao<PrecioProducto, Integer> {

    public PrecioProductoDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(PrecioProducto object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("lista", object.getListaPrecio().getId());
            nuevoRegistro.put("producto", object.getProducto().getId());
            nuevoRegistro.put("precio", object.getPrecio().toString());
            nuevoRegistro.put("unidad", object.getUnidad().getId());
            id = (int) this.getMyDataBase().insert("precio_producto", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<PrecioProducto> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (PrecioProducto object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("lista", object.getListaPrecio().getId());
                nuevoRegistro.put("producto", object.getProducto().getId());
                nuevoRegistro.put("precio", object.getPrecio().toString());
                nuevoRegistro.put("unidad", object.getUnidad().getId());
                Integer id = (int) this.getMyDataBase().insert(
                        "precio_producto", null, nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(PrecioProducto object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("lista", object.getListaPrecio().getId());
            registro.put("producto", object.getProducto().getId());
            registro.put("precio", object.getPrecio().toString());
            registro.put("unidad", object.getUnidad().getId());
            modificados = this.getMyDataBase().update("precio_producto",
                    registro, "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<PrecioProducto> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (PrecioProducto object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("lista", object.getListaPrecio().getId());
                registro.put("producto", object.getProducto().getId());
                registro.put("precio", object.getPrecio().toString());
                registro.put("unidad", object.getUnidad().getId());
                modificados += this.getMyDataBase().update(
                        "precio_producto", registro, "_id=" + object.getId(),
                        null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(PrecioProducto object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("precio_producto",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<PrecioProducto> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (PrecioProducto object : objects) {
                borrados += this.getMyDataBase().delete("precio_producto",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("precio_producto",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<PrecioProducto> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<PrecioProducto> registros = new ArrayList<PrecioProducto>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT _id, producto, unidad, lista, precio FROM precio_producto", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public PrecioProducto getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<PrecioProducto> registros = new ArrayList<PrecioProducto>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, producto, unidad, lista, precio FROM precio_producto WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<PrecioProducto> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<PrecioProducto> registros = new ArrayList<PrecioProducto>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<PrecioProducto> generateList(Cursor c) {
        List<PrecioProducto> registros = new ArrayList<PrecioProducto>();
        if (c.moveToFirst()) {
            do {
                PrecioProducto precio = new PrecioProducto();
                precio.setId(c.getInt(c.getColumnIndex("_id")));
                precio.setPrecio(new BigDecimal(c.getString(c
                        .getColumnIndex("precio"))));

                Cursor d = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre FROM lista_precio WHERE _id="
                                + c.getInt(c.getColumnIndex("lista")), null);
                if (d.moveToFirst()) {
                    ListaPrecio lista = new ListaPrecio();
                    lista.setId(d.getInt(d.getColumnIndex("_id")));
                    lista.setNombre(d.getString(d
                            .getColumnIndex("nombre")));
                    precio.setListaPrecio(lista);
                }

                d = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre, marca, existencia FROM producto WHERE _id="
                                + c.getInt(c.getColumnIndex("producto")), null);
                if (d.moveToFirst()) {
                    Producto producto = new Producto();
                    producto.setId(d.getInt(d.getColumnIndex("_id")));
                    producto.setNombre(d.getString(d.getColumnIndex("nombre")));
                    producto.setMarca(d.getString(d.getColumnIndex("marca")));
                    producto.setExistencia(new BigDecimal(d.getString(d
                            .getColumnIndex("existencia"))));
                    precio.setProducto(producto);
                }

                d = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre, abreviatura, ubase FROM unidad WHERE _id="
                                + c.getInt(c.getColumnIndex("unidad")), null);
                if (d.moveToFirst()) {
                    Unidad unidad = new Unidad();
                    unidad.setId(d.getInt(d.getColumnIndex("_id")));
                    unidad.setNombre(d.getString(d.getColumnIndex("nombre")));
                    unidad.setAbreviatura(d.getString(d
                            .getColumnIndex("abreviatura")));
                    unidad.setUbase(new BigDecimal(d.getString(d
                            .getColumnIndex("ubase"))));
                    precio.setUnidad(unidad);
                }
                d.close();
                registros.add(precio);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }

    public PrecioProducto getPrecio(ListaPrecio lista, Producto producto) {
        this.setMyDataBase(this.getReadableDatabase());
        List<PrecioProducto> registros;
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT  _id, producto, unidad, lista, precio FROM precio_producto WHERE lista="
                        + lista.getId()
                        + " AND producto=" + producto.getId(), null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (!registros.isEmpty()) {
            return registros.get(0);
        } else {
            return null;
        }
    }
}
