/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.maczoft.masmovil.domain.Agenda;
import com.maczoft.masmovil.domain.Bodega;
import com.maczoft.masmovil.domain.Cliente;
import com.maczoft.masmovil.domain.ListaPrecio;
import com.maczoft.masmovil.domain.TipoCliente;
import com.maczoft.masmovil.domain.Usuario;
import com.maczoft.masmovil.repository.BaseDao;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author maczoe
 */
public class AgendaDao extends MoonsideSQLiteHelper implements
        BaseDao<Agenda, Integer> {

    public AgendaDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(Agenda object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("_id", object.getId());
            nuevoRegistro.put("cliente", object.getCliente().getId());
            nuevoRegistro.put("pedido", object.getPedido());
            nuevoRegistro.put("fecha", object.getFecha().toString());
            nuevoRegistro.put("visitado", object.getVisitado());
            nuevoRegistro.put("motivoanula", object.getMotivoanula());
            nuevoRegistro.put("usuario", object.getUsuario().getId());
            if (object.getHora() != null)
                nuevoRegistro.put("hora", object.getHora().toString());
            id = (int) this.getMyDataBase().insert("agenda", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<Agenda> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (Agenda object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("_id", object.getId());
                nuevoRegistro.put("cliente", object.getCliente().getId());
                nuevoRegistro.put("pedido", object.getPedido());
                nuevoRegistro.put("fecha", object.getFecha().toString());
                nuevoRegistro.put("visitado", object.getVisitado());
                nuevoRegistro.put("motivoanula", object.getMotivoanula());
                nuevoRegistro.put("usuario", object.getUsuario().getId());
                if (object.getHora() != null)
                    nuevoRegistro.put("hora", object.getHora().toString());
                Integer id = (int) this.getMyDataBase().insert("agenda", null,
                        nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(Agenda object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("cliente", object.getCliente().getId());
            registro.put("fecha", object.getFecha().toString());
            registro.put("pedido", object.getPedido());
            registro.put("visitado", object.getVisitado());
            registro.put("motivoanula", object.getMotivoanula());
            registro.put("usuario", object.getUsuario().getId());
            if (object.getHora() != null)
                registro.put("hora", object.getHora().toString());
            modificados = this.getMyDataBase().update("agenda", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<Agenda> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (Agenda object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("cliente", object.getCliente().getId());
                registro.put("pedido", object.getPedido());
                registro.put("fecha", object.getFecha().toString());
                registro.put("visitado", object.getVisitado());
                registro.put("motivoanula", object.getMotivoanula());
                registro.put("usuario", object.getUsuario().getId());
                if (object.getHora() != null)
                    registro.put("hora", object.getHora().toString());
                modificados += this.getMyDataBase().update("agenda",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(Agenda object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("agenda",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<Agenda> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (Agenda object : objects) {
                borrados += this.getMyDataBase().delete("agenda",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("agenda",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<Agenda> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Agenda> registros = new ArrayList<Agenda>();
        Cursor c = this
                .getMyDataBase()
                .rawQuery(
                        "SELECT _id, cliente, fecha, visitado, motivoanula, usuario, pedido, hora FROM agenda",
                        null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public Agenda getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Agenda> registros = new ArrayList<Agenda>();
        Cursor c = this
                .getMyDataBase()
                .rawQuery(
                        "SELECT _id, cliente, fecha, visitado, motivoanula, usuario, pedido, hora FROM agenda WHERE _id="
                                + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    public List<Agenda> getByUserDate(java.util.Date date, int usuario)
            throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Agenda> registros = new ArrayList<Agenda>();
        Cursor c = this
                .getMyDataBase()
                .rawQuery(
                        "SELECT _id, cliente, fecha, visitado, motivoanula, usuario, pedido, hora FROM agenda WHERE fecha='"
                                + new Date(date.getTime()).toString()
                                + "' AND usuario=" + usuario + " AND visitado=0", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    public List<Agenda> getVisitados(int usuario)
            throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Agenda> registros = new ArrayList<Agenda>();
        Cursor c = this
                .getMyDataBase()
                .rawQuery(
                        "SELECT _id, cliente, fecha, visitado, motivoanula, usuario, pedido, hora FROM agenda " +
                                "WHERE usuario=" + usuario + " AND visitado=1 AND motivoanula IS NOT NULL", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    public Agenda getByPedido(Integer pedido)
            throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Agenda> registros = new ArrayList<Agenda>();
        Cursor c = this
                .getMyDataBase()
                .rawQuery(
                        "SELECT _id, cliente, fecha, visitado, motivoanula, usuario, pedido, hora FROM agenda WHERE pedido=" + pedido, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<Agenda> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Agenda> registros = new ArrayList<Agenda>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    public void updatePedido(int agenda, int pedido) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL("UPDATE agenda SET pedido=" + pedido + " WHERE _id=" + agenda);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<Agenda> generateList(Cursor c) {
        List<Agenda> registros = new ArrayList<Agenda>();
        if (c.moveToFirst()) {
            do {
                Agenda a = new Agenda();
                a.setId(c.getInt(c.getColumnIndex("_id")));
                a.setVisitado(c.getInt(c.getColumnIndex("visitado")) > 0);
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "yyyy-MM-dd", Locale.getDefault());
                long time = new java.util.Date().getTime();
                try {
                    time = dateFormat.parse(
                            c.getString(c.getColumnIndex("fecha"))).getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Date fecha = new Date(time);
                a.setFecha(fecha);
                dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
                        Locale.getDefault());
                java.util.Date date = null;
                try {
                    if (c.getString(c.getColumnIndex("hora")) != null) {
                        date = dateFormat.parse(c.getString(c
                                .getColumnIndex("hora")));
                        java.sql.Timestamp hora = new java.sql.Timestamp(
                                date.getTime());
                        a.setHora(hora);
                    }
                } catch (ParseException e) {
                    Log.d("Error AgendaDao", e.getLocalizedMessage());
                    e.printStackTrace();
                }
                a.setMotivoanula(c.getString(c.getColumnIndex("motivoanula")));
                Cursor b = this
                        .getMyDataBase()
                        .rawQuery(
                                "SELECT _id, nombre, codigo, nit, direccion, activo, tipo, lista_precio FROM cliente WHERE _id="
                                        + c.getInt(c.getColumnIndex("cliente")),
                                null);
                if (b.moveToFirst()) {
                    Cliente cliente = new Cliente();
                    cliente.setId(b.getInt(b.getColumnIndex("_id")));
                    cliente.setNombre(b.getString(b.getColumnIndex("nombre")));
                    cliente.setCodigo(b.getString(b.getColumnIndex("codigo")));
                    cliente.setNit(b.getString(b.getColumnIndex("nit")));
                    cliente.setDireccion(b.getString(b
                            .getColumnIndex("direccion")));
                    cliente.setActivo(b.getInt(b.getColumnIndex("activo")) > 0);
                    Cursor d = this
                            .getMyDataBase()
                            .rawQuery(
                                    "SELECT _id, nombre FROM lista_precio WHERE _id="
                                            + b.getInt(b
                                            .getColumnIndex("lista_precio")),
                                    null);
                    if (d.moveToFirst()) {
                        ListaPrecio listaPrecio = new ListaPrecio(d.getInt(d
                                .getColumnIndex("_id")), d.getString(d
                                .getColumnIndex("nombre")), "");
                        cliente.setListaPrecio(listaPrecio);
                    }
                    d = this.getMyDataBase().rawQuery(
                            "SELECT _id, nombre FROM tipo_cliente WHERE _id="
                                    + b.getInt(b.getColumnIndex("tipo")), null);
                    if (d.moveToFirst()) {
                        TipoCliente tipoCliente = new TipoCliente(d.getInt(d
                                .getColumnIndex("_id")), d.getString(d.getInt(d
                                .getColumnIndex("nombre"))));
                        cliente.setTipoCliente(tipoCliente);
                    }
                    a.setCliente(cliente);
                    d.close();
                }
                Cursor e = this.getMyDataBase().rawQuery(
                        "SELECT _id, nombre, contrasena, bodega, comision FROM usuario WHERE _id="
                                + c.getInt(c.getColumnIndex("usuario")), null);
                if (e.moveToFirst()) {
                    Usuario usuario = new Usuario(e.getInt(e
                            .getColumnIndex("_id")), e.getString(e
                            .getColumnIndex("nombre")), e.getString(e
                            .getColumnIndex("contrasena")), new BigDecimal(
                            e.getString(e.getColumnIndex("comision"))));
                    Cursor f = this.getMyDataBase().rawQuery(
                            "SELECT _id, nombre FROM bodega WHERE _id="
                                    + e.getInt(e.getColumnIndex("bodega")),
                            null);
                    if (f.moveToFirst()) {
                        Bodega bodega = new Bodega();
                        bodega.setId(f.getInt(f.getColumnIndex("_id")));
                        bodega.setNombre(f.getString(f.getColumnIndex("nombre")));
                        usuario.setBodega(bodega);
                    }
                    a.setUsuario(usuario);
                    f.close();
                }
                registros.add(a);
                b.close();
                e.close();
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }

}
