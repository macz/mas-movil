/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.Unidad;
import com.maczoft.masmovil.repository.BaseDao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class UnidadDao extends MoonsideSQLiteHelper implements
        BaseDao<Unidad, Integer> {

    public UnidadDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(Unidad object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("_id", object.getId());
            nuevoRegistro.put("nombre", object.getNombre());
            nuevoRegistro.put("abreviatura", object.getAbreviatura());
            nuevoRegistro.put("ubase", object.getUbase().toString());

            id = (int) this.getMyDataBase().insert("unidad", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<Unidad> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (Unidad object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("_id", object.getId());
                nuevoRegistro.put("nombre", object.getNombre());
                nuevoRegistro.put("abreviatura", object.getAbreviatura());
                nuevoRegistro.put("ubase", object.getUbase().toString());
                Integer id = (int) this.getMyDataBase().insert("unidad", null,
                        nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(Unidad object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("nombre", object.getNombre());
            registro.put("abreviatura", object.getAbreviatura());
            registro.put("ubase", object.getUbase().toString());
            modificados = this.getMyDataBase().update("unidad", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<Unidad> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (Unidad object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("nombre", object.getNombre());
                registro.put("abreviatura", object.getAbreviatura());
                registro.put("ubase", object.getUbase().toString());
                modificados += this.getMyDataBase().update("unidad",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(Unidad object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("unidad",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<Unidad> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (Unidad object : objects) {
                borrados += this.getMyDataBase().delete("unidad",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("unidad",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<Unidad> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Unidad> registros = new ArrayList<Unidad>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT * FROM unidad", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public Unidad getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Unidad> registros = new ArrayList<Unidad>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, nombre, abreviatura, ubase FROM unidad WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<Unidad> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Unidad> registros = new ArrayList<Unidad>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<Unidad> generateList(Cursor c) {
        List<Unidad> registros = new ArrayList<Unidad>();
        if (c.moveToFirst()) {
            do {
                Unidad unidad = new Unidad();
                unidad.setId(c.getInt(c.getColumnIndex("_id")));
                unidad.setNombre(c.getString(c.getColumnIndex("nombre")));
                unidad.setAbreviatura(c.getString(c
                        .getColumnIndex("abreviatura")));
                unidad.setUbase(new BigDecimal(c.getString(c
                        .getColumnIndex("ubase"))));

                registros.add(unidad);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }
}
