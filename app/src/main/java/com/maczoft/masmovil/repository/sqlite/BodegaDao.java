/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.Bodega;
import com.maczoft.masmovil.repository.BaseDao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class BodegaDao extends MoonsideSQLiteHelper implements
        BaseDao<Bodega, Integer> {

    public BodegaDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(Bodega object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("_id", object.getId());
            nuevoRegistro.put("nombre", object.getNombre());
            id = (int) this.getMyDataBase().insert("bodega", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<Bodega> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (Bodega object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("_id", object.getId());
                nuevoRegistro.put("nombre", object.getNombre());
                Integer id = (int) this.getMyDataBase().insert("bodega", null,
                        nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(Bodega object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("nombre", object.getNombre());
            modificados = this.getMyDataBase().update("bodega", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<Bodega> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (Bodega object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("nombre", object.getNombre());
                modificados += this.getMyDataBase().update("bodega",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(Bodega object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("bodega",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<Bodega> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (Bodega object : objects) {
                borrados += this.getMyDataBase().delete("bodega",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("bodega",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<Bodega> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Bodega> registros = new ArrayList<Bodega>();
        Cursor c = this.getMyDataBase().rawQuery("SELECT _id, nombre FROM bodega", null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public Bodega getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Bodega> registros = new ArrayList<Bodega>();
        Cursor c = this.getMyDataBase().rawQuery(
                "SELECT _id, nombre FROM bodega WHERE _id=" + id, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<Bodega> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Bodega> registros = new ArrayList<Bodega>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<Bodega> generateList(Cursor c) {
        List<Bodega> registros = new ArrayList<Bodega>();
        if (c.moveToFirst()) {
            do {
                Bodega bodega = new Bodega();
                bodega.setId(c.getInt(c.getColumnIndex("_id")));
                bodega.setNombre(c.getString(c.getColumnIndex("nombre")));
                registros.add(bodega);
            } while (c.moveToNext());
        }
        c.close();
        return registros;
    }

}
