/**
 *
 */
package com.maczoft.masmovil.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.maczoft.masmovil.domain.Cliente;
import com.maczoft.masmovil.domain.ListaPrecio;
import com.maczoft.masmovil.domain.TipoCliente;
import com.maczoft.masmovil.repository.BaseDao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maczoe
 */
public class ClienteDao extends MoonsideSQLiteHelper implements
        BaseDao<Cliente, Integer> {

    public ClienteDao(Context context) {
        super(context);
    }

    @Override
    public void startTransaction() throws Exception {
        this.setMyDataBase(this.getWritableDatabase());
        this.getMyDataBase().beginTransaction();
    }

    @Override
    public void commitTransaction() throws Exception {
        this.getMyDataBase().setTransactionSuccessful();
        this.getMyDataBase().endTransaction();
        this.getMyDataBase().close();
    }

    @Override
    public void rollbackTransanction() throws Exception {
        this.getMyDataBase().endTransaction();
    }

    @Override
    public Integer insert(Cliente object) throws Exception {
        Integer id;
        try {
            startTransaction();
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("_id", object.getId());
            nuevoRegistro.put("nombre", object.getNombre());
            nuevoRegistro.put("codigo", object.getCodigo());
            nuevoRegistro.put("nit", object.getNit());
            nuevoRegistro.put("direccion", object.getDireccion());
            nuevoRegistro.put("activo", object.getActivo());
            nuevoRegistro.put("motivo_inactivo", object.getMotivoInactivo());
            if (object.getListaPrecio() != null)
                nuevoRegistro.put("lista_precio", object.getListaPrecio()
                        .getId());
            if (object.getTipoCliente() != null)
                nuevoRegistro.put("tipo", object.getTipoCliente()
                        .getId());
            id = (int) this.getMyDataBase().insert("cliente", null,
                    nuevoRegistro);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return id;
    }

    @Override
    public List<Integer> insert(List<Cliente> objects) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        try {
            startTransaction();

            for (Cliente object : objects) {
                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("_id", object.getId());
                nuevoRegistro.put("nombre", object.getNombre());
                nuevoRegistro.put("codigo", object.getCodigo());
                nuevoRegistro.put("nit", object.getNit());
                nuevoRegistro.put("direccion", object.getDireccion());
                nuevoRegistro.put("activo", object.getActivo());
                nuevoRegistro.put("motivo_inactivo", object.getMotivoInactivo());
                if (object.getListaPrecio() != null)
                    nuevoRegistro.put("lista_precio", object.getListaPrecio()
                            .getId());
                if (object.getTipoCliente() != null)
                    nuevoRegistro.put("tipo", object.getTipoCliente()
                            .getId());
                Integer id = (int) this.getMyDataBase().insert("cliente", null,
                        nuevoRegistro);
                ids.add(id);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo insertar el registro");
        }
        return ids;
    }

    @Override
    public Integer update(Cliente object) throws Exception {
        Integer modificados;
        try {
            startTransaction();
            ContentValues registro = new ContentValues();
            registro.put("nombre", object.getNombre());
            registro.put("codigo", object.getCodigo());
            registro.put("nit", object.getNit());
            registro.put("direccion", object.getDireccion());
            registro.put("activo", object.getActivo());
            registro.put("motivo_inactivo", object.getMotivoInactivo());
            if (object.getListaPrecio() != null)
                registro.put("lista_precio", object.getListaPrecio().getId());
            if (object.getTipoCliente() != null)
                registro.put("tipo", object.getTipoCliente().getId());
            modificados = this.getMyDataBase().update("cliente", registro,
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer update(List<Cliente> objects) throws Exception {
        Integer modificados = 0;
        try {
            startTransaction();

            for (Cliente object : objects) {
                ContentValues registro = new ContentValues();
                registro.put("nombre", object.getNombre());
                registro.put("codigo", object.getCodigo());
                registro.put("nit", object.getNit());
                registro.put("direccion", object.getDireccion());
                registro.put("activo", object.getActivo());
                registro.put("motivo_inactivo", object.getMotivoInactivo());
                if (object.getListaPrecio() != null)
                    registro.put("lista_precio", object.getListaPrecio()
                            .getId());
                if (object.getTipoCliente() != null)
                    registro.put("tipo", object.getTipoCliente()
                            .getId());
                modificados += this.getMyDataBase().update("cliente",
                        registro, "_id=" + object.getId(), null);

            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return modificados;
    }

    @Override
    public Integer delete(Cliente object) throws Exception {
        Integer borrados;
        try {
            startTransaction();
            borrados = this.getMyDataBase().delete("cliente",
                    "_id=" + object.getId(), null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo borrar el registro");
        }
        return borrados;
    }

    @Override
    public Integer delete(List<Cliente> objects) throws Exception {
        Integer borrados = 0;
        try {
            startTransaction();
            for (Cliente object : objects) {
                borrados += this.getMyDataBase().delete("cliente",
                        "_id=" + object.getId(), null);
            }
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo actualizar el registro");
        }
        return borrados;
    }

    @Override
    public void truncate() throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().delete("cliente",
                    null, null);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo truncar los registros");
        }
    }

    @Override
    public List<Cliente> getAll() throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Cliente> registros = new ArrayList<Cliente>();
        String query = "SELECT _id, nombre, codigo, lista_precio, nit, direccion, activo, tipo, motivo_inactivo FROM cliente ORDER BY nombre LIMIT 100";
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    public Cliente getByCodigo(String codigo) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Cliente> registros = new ArrayList<Cliente>();
        String query = "SELECT _id, nombre, codigo, lista_precio, nit, direccion, activo, tipo, motivo_inactivo FROM cliente WHERE codigo='" + codigo + "'";
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public Cliente getById(Integer id) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Cliente> registros = new ArrayList<Cliente>();
        String query = "SELECT _id, nombre, codigo, lista_precio, nit, direccion, activo, tipo, motivo_inactivo FROM cliente WHERE _id=" + id;
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        if (registros.isEmpty())
            return null;
        else
            return registros.get(0);
    }

    @Override
    public List<Cliente> executeQuery(String query) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Cliente> registros = new ArrayList<Cliente>();
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }

    @Override
    public void executUpdate(String query) throws Exception {
        try {
            startTransaction();
            this.getMyDataBase().execSQL(query);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransanction();
            throw new Exception("No se pudo ejecutar consulta");
        }
    }

    private List<Cliente> generateList(Cursor c) {
        List<Cliente> registros = new ArrayList<Cliente>();
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    Cliente cliente = new Cliente();
                    cliente.setId(c.getInt(c.getColumnIndex("_id")));
                    cliente.setNombre(c.getString(c.getColumnIndex("nombre")));
                    cliente.setCodigo(c.getString(c.getColumnIndex("codigo")));
                    cliente.setNit(c.getString(c.getColumnIndex("nit")));
                    cliente.setMotivoInactivo(c.getString(c.getColumnIndex("motivo_inactivo")));
                    cliente.setDireccion(c.getString(c
                            .getColumnIndex("direccion")));
                    cliente.setActivo(c.getInt(c.getColumnIndex("activo")) > 0);

                    Cursor d = this
                            .getMyDataBase()
                            .rawQuery(
                                    "SELECT _id, nombre FROM lista_precio WHERE _id="
                                            + c.getInt(c
                                            .getColumnIndex("lista_precio")),
                                    null);
                    if (d.moveToFirst()) {
                        ListaPrecio listaPrecio = new ListaPrecio(d.getInt(d
                                .getColumnIndex("_id")), d.getString(d
                                .getColumnIndex("nombre")), "");
                        cliente.setListaPrecio(listaPrecio);
                    }
                    d = this.getMyDataBase()
                            .rawQuery(
                                    "SELECT _id, nombre FROM tipo_cliente WHERE _id="
                                            + c.getInt(c
                                            .getColumnIndex("tipo")),
                                    null);
                    if (d.moveToFirst()) {
                        TipoCliente tipoCliente = new TipoCliente(d.getInt(d
                                .getColumnIndex("_id")), d.getString(d.getInt(d
                                .getColumnIndex("nombre"))));
                        cliente.setTipoCliente(tipoCliente);
                    }
                    d.close();
                    registros.add(cliente);
                } while (c.moveToNext());
            }
        }
        c.close();
        return registros;
    }

    public String[] toStringList(List<Cliente> clientes) {
        String[] cadenas = new String[clientes.size()];
        int i = 0;
        for (Cliente c : clientes) {
            cadenas[i] = c.getCodigo() + " - " + c.getNombre();
            i++;
        }
        return cadenas;
    }

    public List<String> getByTarget(String target) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<String> registros = new ArrayList<String>();
        target = target.replace(" ", "%");
        String query = "SELECT nombre, codigo FROM cliente WHERE codigo LIKE '%" + target + "%' OR nombre LIKE '%" + target + "%' LIMIT 10";
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                String cad = c.getString(c.getColumnIndex("codigo")) + " - " + c.getString(c.getColumnIndex("nombre"));
                registros.add(cad);
            } while (c.moveToNext());
        }
        this.getReadableDatabase().close();
        return registros;
    }

    public List<Cliente> getClientesByTarget(String target) throws Exception {
        this.setMyDataBase(this.getReadableDatabase());
        List<Cliente> registros = new ArrayList<Cliente>();
        target = target.replace(" ", "%");
        String query = "SELECT _id, nombre, codigo, lista_precio, nit, direccion, activo, tipo, motivo_inactivo " +
                "FROM cliente WHERE codigo LIKE '%" + target + "%' OR nombre LIKE '%" + target + "%' ORDER BY nombre LIMIT 100";
        Cursor c = this.getMyDataBase().rawQuery(query, null);
        registros = generateList(c);
        this.getReadableDatabase().close();
        return registros;
    }
}
